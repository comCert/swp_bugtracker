#include "bugtablemodel.h"
#include "bug.h"
#include "user.h"

const QVariant BugTableModel::header_names[] = {
    tr("ID"),
    tr("Title"),
    tr("Description"),
    tr("Module"),
    tr("Version"),
    tr("Priority"),
    tr("State"),
    tr("Date"),
    tr("Developer"),
    tr("Tester")
};

BugTableModel::BugTableModel(QObject *parent)
    : QAbstractTableModel(parent)
{
}

BugTableModel::~BugTableModel()
{
    deleteAll();
}

void BugTableModel::addData(Bug * b)
{
    QModelIndex parent;
    int index = bugs.length();

    beginInsertRows(parent, index, index);
    bugs.append(b);
    endInsertRows();
}

void BugTableModel::setData(const QList<Bug *> &b)
{
    beginResetModel();
    deleteAll();
    bugs = b;
    endResetModel();
}

const QList<Bug *> BugTableModel::getData() const
{
    return bugs;
}

int BugTableModel::rowCount(const QModelIndex & parent) const
{
    return bugs.size();
}

int BugTableModel::columnCount(const QModelIndex & parent) const
{
    return BMC_LAST;
}

QVariant BugTableModel::data(const QModelIndex & index,
                             int                 role) const
{
    switch(role)
    {
    case Qt::DisplayRole:
    {
        Bug * b = bugs[index.row()];
        switch(index.column())
        {
        case BMC_ID:          return b->getID();
        case BMC_TITLE:       return b->getTitle();
        case BMC_DESCRIPTION: return b->getDescription();
        case BMC_MODULE:      return b->getModule();
        case BMC_VERSION:     return b->getVersion();
        case BMC_PRIORITY:    return toString(b->getPriority());
        case BMC_STATUS:      return toString(b->getStatus());
        case BMC_DATE:        return b->getDate();
        case BMC_DEVELOPER:   return b->getDeveloper()->getUsername();
        case BMC_TESTER:      return b->getTester()->getUsername();
        }
    }
    case Qt::TextAlignmentRole:
        switch(index.column())
        {
        case BMC_ID:  return Qt::AlignCenter;
        default: return (int)(Qt::AlignLeft | Qt::AlignVCenter);
        }

    default:
        return QVariant();
    }
}

QVariant BugTableModel::headerData(int             section,
                                   Qt::Orientation orientation,
                                   int             role) const
{
    if(role == Qt::DisplayRole && orientation == Qt::Horizontal)
        return header_names[section];
    return QVariant();
}

void BugTableModel::deleteAll()
{
    for(QList<Bug *>::iterator i = bugs.begin(),
        e = bugs.end(); i != e; ++i)
    {
        delete *i;
    }
}
