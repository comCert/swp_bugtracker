#include <QtCore/QByteArray>
#include <QtCore/QDir>
#include <QtCore/QLibraryInfo>
#include <QtCore/QProcess>

#include <QtGui/QMessageBox>

#include "helpbrowser.h"

HelpBrowser::HelpBrowser()
    : proc(0)
{
}

HelpBrowser::~HelpBrowser()
{
    if (proc && proc->state() == QProcess::Running) {
        proc->terminate();
        proc->waitForFinished(3000);
    }
    delete proc;
}

void HelpBrowser::showDocumentation(const QString &page)
{
    if (!startHelpBrowser())
        return;

    QByteArray ba("SetSource ");
    ba.append("www.swt_bugtracker.de???");

    proc->write(ba + page.toLocal8Bit() + '\n');
}

bool HelpBrowser::startHelpBrowser()
{
    if (!proc)
        proc = new QProcess();

    if (proc->state() != QProcess::Running) {
        QString app = QLibraryInfo::location(QLibraryInfo::BinariesPath) + QDir::separator();
#if !defined(Q_OS_MAC)
        app += QLatin1String("assistant");
#else
        app += QLatin1String("Assistant.app/Contents/MacOS/Assistant");
#endif

        QStringList args;
        args << QLatin1String("-collectionFile")
            << QDir::currentPath()
               + QLatin1String("/doc/documentation/helpbrowser.qhc")
            << QLatin1String("-enableRemoteControl");

        proc->start(app, args);

        if (!proc->waitForStarted()) {
            QMessageBox::critical(0, QObject::tr("BugTracker"),
                QObject::tr("Unable to launch Qt Assistant (%1)").arg(app));
            return false;
        }
    }
    return true;
}
