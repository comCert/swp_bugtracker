
#include <QString>
#include <QByteArray>

class CryptoHelper
{
public:

    /// generate random string with 'len' bytes
    static QByteArray generateSalt(int len);

    /// use 'static_salt' and 'salt' to salt 'data' for more secure hashing
    static QByteArray applySalt(const QByteArray & data,
                                const QByteArray & salt);

    /// generate the 20 byte long sha1 hash of 'data'
    static QByteArray generateHash(const QByteArray & data);

    /// test if the hash of 'str' matches 'hash', where the first 20 bytes
    /// of 'hash' contain the actual hash and all bytes after that the salt
    /// used to generate this hash
    static bool       compareHash(const QString & str,
                                  const QByteArray & hash);

private:
    CryptoHelper();

    static const char * static_salt;

};
