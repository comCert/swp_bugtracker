#ifndef DATABASE_H
#define DATABASE_H

#include <QtSql>


class Database
{
public:

    Database(); // invalid object
    ~Database();

    QSqlError createConnection(
        const QString& dbName, const QString& hostName, const unsigned int portNr,
        const QString& userName, const QString& passwd);
    QSqlDatabase getConnection(); // returns the database or an empty one
    void      showError(const QSqlError& e);

    bool         performQuery(const QString & query, QSqlQuery * q = NULL);
    bool         performQuery(const QString & query, const QVariant & param1,
                              QSqlQuery * q = NULL);
    bool         performQuery(const QString & query, const QVariant & param1,
                              const QVariant & param2, QSqlQuery * q = NULL);
    bool         performQuery(const QString & query, const QVariant & param1,
                              const QVariant & param2, const QVariant & param3,
                              QSqlQuery * q = NULL);
    bool         performQuery(const QString & query, const QVariant & param1,
                              const QVariant & param2, const QVariant & param3,
                              const QVariant & param4, QSqlQuery * q = NULL);
    bool         performQuery(const QString & query, const QVariant & param1,
                              const QVariant & param2, const QVariant & param3,
                              const QVariant & param4, const QVariant & param5,
                              const QVariant & param6, QSqlQuery * q = NULL);

private:

    QSqlError createTables(); // could be made editable by e.g. parsing a .conf file where all is defined
    QSqlError createPermissions();
    QSqlError fillExamples(); // debugging use only

    QSqlDatabase db;
    bool         isTestCreation;

    QString      database;
    QString      host;
    unsigned int port;
    QString      user;
    QString      password;

};

#endif // DATABASE_H
