#ifndef DTIME_H
#define DTIME_H

#include <QTime>
#include <QDate>
#include <QDateTime>

class DTime
{
public:
    static QString nowToISO8601();

private:
    DTime();
};

#endif // DTIME_H
