#ifndef BUGTABLEMODEL_H
#define BUGTABLEMODEL_H

#include <QAbstractTableModel>
#include <QList>

class Bug;

enum BugTableModelColum
{
    BMC_ID = 0,
    BMC_TITLE,
    BMC_DESCRIPTION,
    BMC_MODULE,
    BMC_VERSION,
    BMC_PRIORITY,
    BMC_STATUS,
    BMC_DATE,
    BMC_DEVELOPER,
    BMC_TESTER,
    BMC_LAST
};

/// data model to display Bug objects in QTableView widgets
class BugTableModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit BugTableModel(QObject *parent = 0);
    virtual ~BugTableModel();

    void addData(Bug * b);
    void setData(const QList<Bug *> &bugs);
    const QList<Bug *> getData() const;

    virtual int rowCount(const QModelIndex & parent = QModelIndex()) const;
    virtual int columnCount(const QModelIndex & parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

private:
    void deleteAll();
    QList<Bug *> bugs;
    static const QVariant header_names[BMC_LAST];

signals:

public slots:

};

#endif // BUGTABLEMODEL_H
