
#include <QCryptographicHash>
#include "cryptohelper.h"

const char * CryptoHelper::static_salt = "0GFtjCKCZilc4U3S";

QByteArray CryptoHelper::generateSalt(int len)
{
    QByteArray salt;

    salt.resize(len);
    for(int i = 0; i < len; ++i)
        salt[i] = qrand() % 256;

    return salt;
}

QByteArray CryptoHelper::applySalt(const QByteArray & data,
                                   const QByteArray & salt)
{
    QByteArray d;

    d.append(static_salt, 16);
    d.append(data);
    d.append(salt);

    return d;
}

QByteArray CryptoHelper::generateHash(const QByteArray & data)
{
    return QCryptographicHash::hash(data, QCryptographicHash::Sha1);
}

bool CryptoHelper::compareHash(const QString & str,
                               const QByteArray & hash)
{
    QByteArray salt = hash.mid(20);
    return hash.mid(0, 20) == CryptoHelper::applySalt(str.toAscii(), salt);
}
