#ifndef QUERYBUILDER_H
#define QUERYBUILDER_H

#include <QList>
#include <QSqlQuery>

enum QueryTarget {
    QT_USER,
    QT_PROJECT,
    QT_BUG,
    QT_COMMENT,
    QT_ATTACHEMENT
};

enum QueryOperation {
    QO_INSERT,
    QO_UPDATE,
    QO_DELETE
};

class QueryBuilder
{
public:
    QueryBuilder();
    QueryBuilder(const QList<QString>& list);
    QString getGeneratedQuery();
    //QSqlQuery getGeneratedQuery();

private:
    void generateQuery(const QList<QString>& l);

    QString query;
    QSqlQuery sqlQuery;
    bool listIsValid;
};

#endif // QUERYBUILDER_H
