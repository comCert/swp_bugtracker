#ifndef SESSION_H
#define SESSION_H

#include <QString>
#include "core/user.h"
#include "util/database.h"

class Session : public QObject
{
    Q_OBJECT

public:

    static void       init();
    static bool       login(const QString &username, const QString &password);
    static void       logout();
    static User     & getUser()     { return *user;    }
    static Database & getDatabase() { return database; }

    static Session instance;

signals:

    void loggedIn(User*);

private:

    void emitLogin();

    static User   * user;
    static Database database;
    static bool     first_login;
};

inline bool hasPerm(const QString & perm)
{
    return Session::getUser().hasPermission(perm);
}

#endif // SESSION_H
