#include "database.h"
#include "user.h"
#include "bug.h"

#include <QtGui>

Database::Database()
{
    // enables test mode: insertion of tables and examples
    isTestCreation = true;
}

Database::~Database()
{
    db.close();
}

QSqlError Database::createConnection(const QString& dbName,
                                     const QString& hostName,
                                     const unsigned int portNr,
                                     const QString& userName,
                                     const QString& passwd)
{
    database   = dbName;
    host       = hostName;
    port       = portNr;
    user       = userName;
    password   = passwd;

    if (!db.isDriverAvailable("QSQLITE")) {
        QSqlError e;
        e.setType(QSqlError::ConnectionError);
        e.setDatabaseText("Driver not found!");
        return e;
    }

    // Host specific part of connection ------------------
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(database);
    db.setHostName(host);
    db.setPort(port);

    // User specific part --------------------------------
    db.setUserName(user);
    db.setPassword(password);
    if (user == "guest")
        db.setConnectOptions("QSQLITE_OPEN_READONLY=1");

    db.open();

    if (isTestCreation) {
        showError(createTables());
        showError(createPermissions());
        showError(fillExamples());
    }

    return db.lastError();
}

QSqlDatabase Database::getConnection()
{
    if (db.isValid() && db.isOpen())
        return db;
    else
        return QSqlDatabase(); // invalid creation
}

void Database::showError(const QSqlError &e)
{
    if (e.type() != QSqlError::NoError) {
        QMessageBox::critical(0, QObject::tr("Database error"),
                              QObject::tr("A database error occured:\n\n") + e.text(),
                              QMessageBox::Cancel, QMessageBox::NoButton);
    }
}

/*
 * QSQLite - Datatypes:
 *
 * SQLite v3 data type      SQL type description                                    recommended input
 * ---------------------------------------------------------------------------------------------------------------------
 *  NULL                    NULL value                                              NULL
 *  INTEGER                 Signed integer, stored in 8, 16, 24, 32, 48,            typedef qint8/16/32/64
 *                          or 64-bits depending on the magnitude of the value.
 *  REAL                    64-bit floating point value                             by default mapping to QString
 *  TEXT                    Character string (UTF-8, UTF-16BE or UTF-16-LE).        mapped to QString
 *  CLOB                    Character large string object                           mapped to QString
 *  BLOB                    The value is a BLOB of data, stored exactly             mapped to QByteArray
 *                          as it was input.
 */

// dates will be stored in TEXT as ISO8601 strings ("YYYY-MM-DD HH:MM:SS.SSS")
// boolean values are stored in INTEGER as 0 (false) and 1 (true)

QSqlError Database::createTables()
{
    QSqlQuery q;

    // 'pass' will always consist of 20 bytes hash + 8 bytes salt
    if (!q.exec("CREATE TABLE IF NOT EXISTS user ("
                    "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                    "username TEXT,"
                    "pass TEXT,"
                    "first_name TEXT,"
                    "last_name TEXT,"
                    "email TEXT,"
                    "type INTEGER,"
                    "lastLogin TEXT,"
                    "lastLogout TEXT)"))
        return q.lastError();
    if (!q.exec("CREATE TABLE IF NOT EXISTS project ("
                    "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                    "name TEXT)"))
        return q.lastError();
    // - Each UserType (see user.h and table user) is associated with a number of
    // permissions by name, e.g. admin -> "user.create"
    // - see createPermissions() for more
    if (!q.exec("CREATE TABLE IF NOT EXISTS permission ("
                    "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                    "user_type INTEGER,"
                    "perm_name TEXT)"))
        return q.lastError();
    if (!q.exec("CREATE TABLE IF NOT EXISTS bug ("
                    "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                    "title TEXT,"
                    "description TEXT,"
                    "module TEXT,"
                    "version TEXT,"
                    "date TEXT,"
                    "state INTEGER,"
                    "priority INTEGER,"
                    "tester_id INTEGER,"
                    "developer_id INTEGER,"
                    "FOREIGN KEY(tester_id) REFERENCES user(id),"
                    "FOREIGN KEY(developer_id) REFERENCES user(id))"))
        return q.lastError();
    if (!q.exec("CREATE TABLE IF NOT EXISTS comment ("
                    "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                    "author_id INTEGER,"
                    "bug_id INTEGER,"
                    "date TEXT,"
                    "text TEXT,"
                    "FOREIGN KEY(author_id) REFERENCES user(id),"
                    "FOREIGN KEY(bug_id) REFERENCES bug(id))"))
        return q.lastError();
    if (!q.exec("CREATE TABLE IF NOT EXISTS attachment ("
                    "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                    "name TEXT,"
                    "uploader_id INTEGER,"
                    "bug_id INTEGER,"
                    "date TEXT,"
                    "mime_type TEXT,"
                    "data BLOB,"
                    "FOREIGN KEY(uploader_id) REFERENCES user(id),"
                    "FOREIGN KEY(bug_id) REFERENCES bug(id))"))
        return q.lastError();
    // ... to be continued

    return QSqlError(); // no error occured
}

QSqlError Database::createPermissions()
{
    QSqlQuery q;

    // multi-row inserts should be supported by sqlite
    // see https://www.sqlite.org/lang_insert.html
    q.exec("INSERT INTO permission (user_type, perm_name) VALUES "
            // guest

            // admin
            "(1, 'user.create'), "
            "(1, 'user.change-type'), "
            "(1, 'user.delete'), "
            "(1, 'bug.delete'), "
            "(1, 'bug.change-status'), "
            "(1, 'bug.change-priority'), "
            "(1, 'bug.change-developer'), "
            "(1, 'comment.delete'), "
            "(1, 'attachment.delete'), "
            "(1, 'comment.create'), "
            "(1, 'attachment.create'), "

            // developer
            "(2, 'bug.change-status'), "
            "(2, 'comment.create'), "
            "(2, 'attachment.create'), "

            // chief developer
            "(3, 'bug.change-status'), "
            "(3, 'bug.change-priority'), "
            "(3, 'bug.change-developer'), "
            "(3, 'comment.create'), "
            "(3, 'attachment.create'), "

            // tester
            "(4, 'bug.create'), "
            "(4, 'bug.change-status'), "
            "(4, 'bug.change-priority'), "
            "(4, 'comment.create'), "
            "(4, 'attachment.create')");

    // using bindValue instead of hardcoded values yields an
    // 'parameter count mismatch' error

    //q.bindValue(":ut_guest"          , UT_GUEST);
    //q.bindValue(":ut_admin"          , UT_ADMIN);
    //q.bindValue(":ut_developer"      , UT_DEVELOPER);
    //q.bindValue(":ut_chief_developer", UT_CHIEF_DEVELOPER);
    //q.bindValue(":ut_tester"         , UT_TESTER);
    //q.exec();

    return q.lastError();
}

QSqlError Database::fillExamples()
{
    QSqlQuery q;

    if(!q.prepare(QLatin1String("insert into project(name) values (?)")))
        return q.lastError();
    q.addBindValue("BugTracker");
    if(!q.exec())
        return q.lastError();

    // user
    User * guest, * admin, * tester1, * tester2, * dev1, * dev2, * cdev;

    guest   = User::createUser("guest", "guest", UT_GUEST);
    admin   = User::createUser("admin", "root", UT_ADMIN);
    admin->changeInfo("Peter", "Kilometer", "p_kilo@meter.net");
    tester1 = User::createUser("testi", "123", UT_TESTER);
    tester2 = User::createUser("quizi", "123", UT_TESTER);
    dev1    = User::createUser("dev1", "123", UT_DEVELOPER);
    dev2    = User::createUser("dev2", "123", UT_DEVELOPER);
    cdev    = User::createUser("chefi", "123", UT_CHIEF_DEVELOPER);

    // bugs
    QString lorem = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.";
    Bug * b;

    b = new Bug("Bug 1", lorem, "GUI", "0.15.2", BP_HIGH);
    tester1->reportBug(b); delete b;

    b = new Bug("Bug 2", lorem, "Core", "0.16.1", BP_DEFAULT);
    tester1->reportBug(b); delete b;

    b = new Bug("Bug 3", lorem, "GUI", "0.15.3", BP_LOW);
    tester2->reportBug(b); delete b;

    /*if(!q.prepare(QLatin1String("insert into bug(title, description, module, version, date, state, priority, "
                                "tester_id, developer_id) values (?, ?, ?, ?, ?, ?, ?, ?, ?)")))
        return q.lastError();
    q.addBindValue("resize error");
    q.addBindValue("there are no window bounds while resizing!");
    q.addBindValue("UI");
    q.addBindValue("0.93.1");
    q.addBindValue("2013-01-20 08:37:20.001");
    q.addBindValue("open");
    q.addBindValue(3);
    q.addBindValue(1);
    q.addBindValue(1);
    if(!q.exec())
        return q.lastError();*/

    return QSqlError(); // no error occured
}

bool Database::performQuery(const QString & query_str,
                            QSqlQuery * q)
{
    QSqlQuery query;

    if(q == NULL) q = &query;
    if(!q->exec(query_str)) {
        showError(q->lastError());
        return false;
    }
    return true;
}

bool Database::performQuery(const QString & query_str,
                            const QVariant & p1,
                            QSqlQuery * q)
{
    QSqlQuery query;

    if(q == NULL) q = &query;
    q->prepare(query_str);
    q->addBindValue(p1);
    if(!q->exec()) {
        showError(q->lastError());
        return false;
    }
    return true;
}

bool Database::performQuery(const QString & query_str,
                            const QVariant & p1,
                            const QVariant & p2,
                            QSqlQuery * q)
{
    QSqlQuery query;

    if(q == NULL) q = &query;
    q->prepare(query_str);
    q->addBindValue(p1);
    q->addBindValue(p2);
    if(!q->exec()) {
        showError(q->lastError());
        return false;
    }
    return true;
}

bool Database::performQuery(const QString & query_str,
                            const QVariant & p1,
                            const QVariant & p2,
                            const QVariant & p3,
                            QSqlQuery * q)
{
    QSqlQuery query;

    if(q == NULL) q = &query;
    q->prepare(query_str);
    q->addBindValue(p1);
    q->addBindValue(p2);
    q->addBindValue(p3);
    if(!q->exec()) {
        showError(q->lastError());
        return false;
    }
    return true;
}

bool Database::performQuery(const QString & query_str,
                            const QVariant & p1,
                            const QVariant & p2,
                            const QVariant & p3,
                            const QVariant & p4,
                            QSqlQuery * q)
{
    QSqlQuery query;

    if(q == NULL) q = &query;
    q->prepare(query_str);
    q->addBindValue(p1);
    q->addBindValue(p2);
    q->addBindValue(p3);
    q->addBindValue(p4);
    if(!q->exec()) {
        showError(q->lastError());
        return false;
    }
    return true;
}

bool Database::performQuery(const QString & query_str,
                            const QVariant & p1,
                            const QVariant & p2,
                            const QVariant & p3,
                            const QVariant & p4,
                            const QVariant & p5,
                            const QVariant & p6,
                            QSqlQuery * q)
{
    QSqlQuery query;

    if(q == NULL) q = &query;
    q->prepare(query_str);
    q->addBindValue(p1);
    q->addBindValue(p2);
    q->addBindValue(p3);
    q->addBindValue(p4);
    q->addBindValue(p5);
    q->addBindValue(p6);
    if(!q->exec()) {
        showError(q->lastError());
        return false;
    }
    return true;
}
