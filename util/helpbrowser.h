#ifndef HELPBROWSER_H
#define HELPBROWSER_H

#include <QString>

class QProcess;

class HelpBrowser
{
public:
    HelpBrowser();
    ~HelpBrowser();
    void showDocumentation(const QString &file);

private:
    bool startHelpBrowser();
    QProcess *proc;
};

#endif // HELPBROWSER_H
