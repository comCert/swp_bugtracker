#include "dtime.h"

QString DTime::nowToISO8601()
{
    QString time = QDateTime::currentDateTime().toString(Qt::ISODate);
    time.replace(QString("T"), QString(" ")); // rem. 'T' in between and write a blank there

    return time;
}
