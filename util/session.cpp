#include "session.h"
#include "user.h"
#include "database.h"
#include "cryptohelper.h"
#include "dtime.h"

#include <QSqlQuery>
#include <QSettings>

User   * Session::user;
Database Session::database;
Session  Session::instance;
bool     Session::first_login = true;

bool Session::login(const QString &username, const QString &password)
{
    QSettings conf("BugTracker", QSettings::NativeFormat);


    if(user != NULL && user->getUsername() == username)
        return true;

    if(first_login)
    {
        // connect to database
        database.createConnection(
            conf.value("connection/db").toString(),
            conf.value("connection/host").toString(),
            conf.value("connection/port").toInt(),
            username, password
        );
    }

    // check if password is corrent
    QSqlQuery  q;
    if(!database.performQuery("SELECT id, pass "
                              "FROM   user "
                              "WHERE  username=?", username, &q))
        return false;
    if(!q.next())
        return false;
    //if(!CryptoHelper::compareHash(password, q.value(1).toByteArray()))
    if(password != q.value(1).toString())
        return false;

    // login succeeded; load user information
    if(first_login)
    {
        User::loadPermissions();
        User::loadMinimalUserInfo();
    }
    user = User::getByID( q.value(0).toInt() );
    user->loadInfo();

    // update lastLogin timestamp
    database.performQuery(
        "UPDATE user SET lastLogin=? WHERE id=?",
        DTime::nowToISO8601(), user->getID());

    first_login = false;
    Session::instance.emitLogin();
    return true;
}

void Session::logout()
{
    // update lastLogout timestamp
    database.performQuery(
        "UPDATE user SET lastLogout=? WHERE id=?",
        DTime::nowToISO8601(), user->getID());

    // revert user to guest
    Session::login("guest", "guest");
}

void Session::emitLogin()
{
    emit loggedIn(user);
}
