#include "attachmenttablemodel.h"
#include "attachment.h"
#include "user.h"

AttachmentTableModel::AttachmentTableModel(QObject *parent)
    : QAbstractTableModel(parent)
{
}

AttachmentTableModel::~AttachmentTableModel()
{
    deleteAll();
}

void AttachmentTableModel::addData(Attachment * a)
{
    QModelIndex parent;
    int index = attachments.length();

    beginInsertRows(parent, index, index);
    attachments.append(a);
    endInsertRows();
}

void AttachmentTableModel::setData(const QList<Attachment *> &atts)
{
    beginResetModel();
    deleteAll();
    attachments = atts;
    endResetModel();
}

const QList<Attachment *> & AttachmentTableModel::getData() const
{
    return attachments;
}

int AttachmentTableModel::rowCount(const QModelIndex & parent) const
{
    return attachments.size();
}

int AttachmentTableModel::columnCount(const QModelIndex & parent) const
{
    return 5;
}

QVariant AttachmentTableModel::data(const QModelIndex & index,
                                    int                 role) const
{
    switch(role)
    {
    case Qt::DisplayRole:
    {
        Attachment * a = attachments[index.row()];
        switch(index.column())
        {
        case 0: return a->getID();
        case 1: return a->getName();
        case 2: return a->getDate();
        case 3: return a->getMimeType();
        case 4: return a->getUploader()->getUsername();
        }
    }
    case Qt::TextAlignmentRole:
        switch(index.column())
        {
        case 0:  return Qt::AlignCenter;
        default: return (int)(Qt::AlignLeft | Qt::AlignVCenter);
        }

    default:
        return QVariant();
    }
}

QVariant AttachmentTableModel::headerData(
    int             section,
    Qt::Orientation orientation,
    int             role) const
{
    if(role == Qt::DisplayRole && orientation == Qt::Horizontal)
        switch(section)
        {
        case 0: return tr("ID");
        case 1: return tr("Filename");
        case 2: return tr("Date");
        case 3: return tr("Mime Type");
        case 4: return tr("Uploader");
        }

    return QVariant();
}

void AttachmentTableModel::deleteAll()
{
    for(QList<Attachment *>::iterator i = attachments.begin(),
        e = attachments.end(); i != e; ++i)
    {
        delete *i;
    }
}
