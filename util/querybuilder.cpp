#include "querybuilder.h"

QueryBuilder::QueryBuilder()
{
}

QueryBuilder::QueryBuilder(const QList<QString> &list)
{
    if (list.isEmpty())
        listIsValid = false;
    else
        listIsValid = true;
}

QString QueryBuilder::getGeneratedQuery()
{
    return query;
}

/*QSqlQuery QueryBuilder::getGeneratedQuery()
{
    return sqlQuery;
}*/

void QueryBuilder::generateQuery(const QList<QString> &l)
{
    if (!listIsValid) {
        query = "";
        return;
    }

    // generate query out of list with some strategy
    // ... TODO
    switch (l.first().toInt()) {
    case QT_USER        : break;
    case QT_PROJECT     : break;
    case QT_BUG         : break;
    case QT_COMMENT     : break;
    case QT_ATTACHEMENT : break;
    default             : return;
        break;
    };

    switch (l.value(1).toInt()) {
    case QO_DELETE : break;
    case QO_INSERT : break;
    case QO_UPDATE : break;
    default        : return;
        break;
    }
}
