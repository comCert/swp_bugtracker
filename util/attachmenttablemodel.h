#ifndef ATTACHMENTTABLEMODEL_H
#define ATTACHMENTTABLEMODEL_H

#include <QAbstractTableModel>
#include <QList>

class Attachment;

/// data model to display Attachment objects in QTableView widgets
class AttachmentTableModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit AttachmentTableModel(QObject *parent = 0);
    virtual ~AttachmentTableModel();

    void addData(Attachment *);
    void setData(const QList<Attachment *> &);
    const QList<Attachment *> & getData() const;

    virtual int rowCount(const QModelIndex & parent = QModelIndex()) const;
    virtual int columnCount(const QModelIndex & parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

private:
    void deleteAll();
    QList<Attachment *> attachments;

};

#endif // ATTACHMENTTABLEMODEL_H
