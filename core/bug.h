#ifndef BUG_H
#define BUG_H

#include <QList>
#include <QString>
#include <QDateTime>
#include <QSqlQuery>

// forward declarations
class User;
class Comment;
class Attachment;

enum BugPriority
{
    BP_LOW = 0,
    BP_DEFAULT,
    BP_HIGH,
    BP_URGENT
};

enum BugStatus
{
    BS_NOT_ASSIGNED = 0,
    BS_ASSIGNED,
    BS_FIXING,
    BS_FIXED,
    BS_CLOSED
};

BugPriority toBugPriority(int bp);
BugStatus toBugStatus(int bs);

QString toString(BugPriority);
QString toString(BugStatus);


class Bug
{
public:

    Bug(const QString & title, const QString & desc, const QString & module,
        const QString & version, BugPriority bp);
    Bug(const QSqlQuery & q, const int map[] = NULL);

    bool loadById(int bug_id);
    bool remove();
    bool changePriority(BugPriority prio);
    bool changeStatus(BugStatus status);

    QList<Comment *> getComments() const;
    Comment * addComment(const QString & comment);

    QList<Attachment *> getAttachments() const;
    Attachment * addAttachment(const QString & path);

    // getter
    int               getID() const;
    const QString   & getTitle() const;
    const QString   & getDescription() const;
    const QString   & getModule() const;
    const QString   & getVersion() const;
    const QDateTime & getDate() const;
    BugStatus         getStatus() const;
    BugPriority       getPriority() const;
    User *            getTester() const;
    User *            getDeveloper() const;

    // setter
    void setID(int id);
    void setTitle(const QString &title);
    void setDescription(const QString &description);
    void setModule(const QString &module);
    void setVersion(const QString &version);
    void setStatus(BugStatus status);
    void setPriority(BugPriority priority);
    void setTester(int tester_id);
    void setDeveloper(int dev_id);

private:

    int         id;
    QString     title;
    QString     description;
    QString     module;
    QString     version;
    QDateTime   date;
    BugStatus   status;
    BugPriority priority;
    User *      tester;
    User *      developer;

};

#endif // BUG_H
