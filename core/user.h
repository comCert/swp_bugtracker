#ifndef USER_H
#define USER_H

#include <QList>
#include <QString>
#include <QDateTime>
#include <QSet>
#include <QMap>

// forward declarations
class Bug;

// user roles
enum UserType
{
    UT_GUEST = 0,
    UT_ADMIN,
    UT_DEVELOPER,
    UT_CHIEF_DEVELOPER,
    UT_TESTER,
    UT_LAST
};

QString toString(UserType);

class User
{
public:

    // fill the static 'permissions'-member with data from database
    static void loadPermissions();

    // fill the static 'user'-member with with minimal data to allow for a
    // id-to-username mapping
    static void loadMinimalUserInfo();

    // map 'user_id' to the corrosponding user-object
    static User * getByID(int user_id);

    // create a new user and add him to the database
    // returns: minimal filled pointer to the newly created user
    static User * createUser(const QString & username,
                             const QString & password,
                             UserType        type);

    // retrieve additional info about this from database
    // (fills first_name, last_name, email, lastLogin/-Logout, type)
    bool loadInfo();

    // change personal information
    // returns: true if database request succeeded, false otherwise
    bool changeInfo(const QString & first_name,
                    const QString & last_name,
                    const QString & email);

    // change user-type/-role to 'type'
    // returns: true if database request succeeded, false otherwise
    bool changeType(UserType type);
    void changePassword(const QString & password,
                        const QString & old_password);

    // check if this user is allowed to perform the action denoted by 'perm'
    bool hasPermission(const QString & perm) const;

    // retrieve a list of all bugs associated with this user
    QList<Bug *> getBugs() const;

    // report 'bug' and add it to database
    // only allow if type == UT_TESTER
    bool reportBug(Bug *bug);

    // assign 'bug' to this user
    bool assignBug(Bug *bug);

    // getter
    int               getID        () const;
    const QString   & getUsername  () const;
    const QString   & getFirstName () const;
    const QString   & getLastName  () const;
    const QString   & getEmail     () const;
    const QDateTime & getLastLogin () const;
    const QDateTime & getLastLogout() const;
    UserType          getType      () const;

private:

    static QSet<QString> permissions[UT_LAST];
    static QMap<int, User> users;

    int       id;
    QString   username;
    QString   first_name;
    QString   last_name;
    QString   email;
    QDateTime lastLogin;
    QDateTime lastLogout;
    UserType  type;

};

#endif // USER_H
