#include "comment.h"
#include "session.h"
#include "bug.h"
#include "user.h"

#include <QVariant>


Comment::Comment(const QString & text)
    : id(-1)
    , author(&Session::getUser())
    , date(QDateTime::currentDateTime())
    , text(text)
{
}

Comment::Comment(const QSqlQuery & q, const int map[])
{
    if(map == NULL)
    {
        static const int m[] = {0,1,2,3};
        map = m;
    }

    id     = q.value(map[0]).toInt();
    author = User::getByID( q.value(map[1]).toInt() );
    date   = q.value(map[2]).toDateTime();
    text   = q.value(map[3]).toString();
}

bool Comment::edit(const QString & new_text)
{
    if(Session::getDatabase().performQuery(
        "UPDATE comment SET text=? WHERE  id=?",
        new_text, getID()))
    {
        text = new_text;
        return true;
    }
    return false;
}

bool Comment::remove()
{
    return Session::getDatabase().performQuery(
        "DELETE FROM comment WHERE id=?", getID());
}

int Comment::getID() const
{
    return id;
}

User * Comment::getAuthor() const
{
    return author;
}

// ~Bug * Comment::getBug() const
// ~{
    // ~return bug;
// ~}

const QDateTime & Comment::getDate() const
{
    return date;
}

const QString & Comment::getText() const
{
    return text;
}

void Comment::setID(int id_)
{
    id = id_;
}
