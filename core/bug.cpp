#include "bug.h"
#include "session.h"
#include "user.h"
#include "comment.h"
#include "attachment.h"
#include "dtime.h"

#include <QVariant>


BugPriority toBugPriority(int bp)
{
    switch(bp)
    {
    case BP_LOW:     return BP_LOW;
    default:
    case BP_DEFAULT: return BP_DEFAULT;
    case BP_HIGH:    return BP_HIGH;
    case BP_URGENT:  return BP_URGENT;
    }
}

BugStatus toBugStatus(int bs)
{
    switch(bs)
    {
    default:
    case BS_NOT_ASSIGNED: return BS_NOT_ASSIGNED;
    case BS_ASSIGNED:     return BS_ASSIGNED;
    case BS_FIXING:       return BS_FIXING;
    case BS_FIXED:        return BS_FIXED;
    case BS_CLOSED:       return BS_CLOSED;
    }
}

QString toString(BugPriority bp)
{
    switch(bp)
    {
    case BP_LOW:     return QObject::tr("low");
    default:
    case BP_DEFAULT: return QObject::tr("default");
    case BP_HIGH:    return QObject::tr("high");
    case BP_URGENT:  return QObject::tr("urgent");
    }
}

QString toString(BugStatus bs)
{
    switch(bs)
    {
    default:
    case BS_NOT_ASSIGNED: return QObject::tr("not assigned");
    case BS_ASSIGNED:     return QObject::tr("assigned");
    case BS_FIXING:       return QObject::tr("fixing");
    case BS_FIXED:        return QObject::tr("fixed");
    case BS_CLOSED:       return QObject::tr("closed");
    }
}


Bug::Bug(const QString & title, const QString & desc, const QString & module,
         const QString & version, BugPriority bp)
    : title    (title)
    , description(desc)
    , module   (module)
    , version  (version)
    , date     (QDateTime::currentDateTime())
    , status   (BS_NOT_ASSIGNED)
    , priority (bp)
    , tester   (User::getByID(-1))
    , developer(User::getByID(-1))
{
}

Bug::Bug(const QSqlQuery & q, const int map[])
{
    if(map == NULL)
    {
        const static int m[] = {0,1,2,3,4,5,6,7,8,9};
        map = m;
    }

    id          = q.value(map[0]).toInt();
    title       = q.value(map[1]).toString();
    description = q.value(map[2]).toString();
    module      = q.value(map[3]).toString();
    version     = q.value(map[4]).toString();
    date        = q.value(map[5]).toDateTime();
    status      = toBugStatus  ( q.value(map[6]).toInt() );
    priority    = toBugPriority( q.value(map[7]).toInt() );
    tester      = User::getByID( q.value(map[8]).toInt() );
    developer   = User::getByID( q.value(map[9]).toInt() );
}


bool Bug::loadById(int bug_id)
{
    QSqlQuery q;

    if(Session::getDatabase().performQuery(
        "SELECT   title, description, module, version, date,"
        "         state, priority, tester_id, developer_id "
        "FROM     bug "
        "WHERE    id=?", bug_id, &q))
    {
        if(!q.next())
            return false;
        id          = bug_id;
        title       = q.value(0).toString();
        description = q.value(1).toString();
        module      = q.value(2).toString();
        version     = q.value(3).toString();
        date        = q.value(4).toDateTime();
        status      = toBugStatus  ( q.value(5).toInt() );
        priority    = toBugPriority( q.value(6).toInt() );
        tester      = User::getByID( q.value(7).toInt() );
        developer   = User::getByID( q.value(8).toInt() );
        return true;
    }
    return false;
}

bool Bug::remove()
{
    return Session::getDatabase().performQuery(
        "DELETE FROM bug WHERE id=?", getID());
}

bool Bug::changePriority(BugPriority new_priority)
{
    if(Session::getDatabase().performQuery(
        "UPDATE bug SET priority=? WHERE id=?",
        new_priority, getID()))
    {
        priority = new_priority;
        return true;
    }
    return false;
}

bool Bug::changeStatus(BugStatus new_status)
{
    if(Session::getDatabase().performQuery(
        "UPDATE bug SET state=? WHERE id=?",
        new_status, getID()))
    {
        status = new_status;
        return true;
    }
    return false;
}

QList<Comment *> Bug::getComments() const
{
    QSqlQuery        q;
    QList<Comment *> l;

    if(Session::getDatabase().performQuery(
        "SELECT   id, author_id, date, text, bug_id "
        "FROM     comment "
        "WHERE    bug_id=? "
        "ORDER BY date ASC", getID(), &q))
    {
        while(q.next())
            l.append(new Comment(q));
    }
    return l;
}

Comment * Bug::addComment(const QString & comment)
{
    QSqlQuery q;
    Comment * c = new Comment(comment);

    if(Session::getDatabase().performQuery(
        "INSERT INTO comment (author_id, date, text, bug_id) "
        "VALUES (?, ?, ?, ?)", c->getAuthor()->getID(),
        c->getDate(), c->getText(), getID(), &q))
    {
        c->setID( q.lastInsertId().toInt() );
        return c;
    }
    delete c;
    return NULL;
}

QList<Attachment *> Bug::getAttachments() const
{
    QSqlQuery           q;
    QList<Attachment *> l;

    if(Session::getDatabase().performQuery(
        "SELECT   id, uploader_id, date, name, mime_type "
        "FROM     attachment "
        "WHERE    bug_id=? "
        "ORDER BY date ASC", getID(), &q))
    {
        while(q.next())
            l.append(new Attachment(q));
    }
    return l;
}

Attachment * Bug::addAttachment(const QString & path)
{
    QFile file(path);
    if(!file.open(QIODevice::ReadOnly))
        return NULL;

    QSqlQuery q;
    QByteArray data  = file.readAll();
    Attachment * att = new Attachment(path);

    if(Session::getDatabase().performQuery(
        "INSERT INTO attachment (name, uploader_id, bug_id, date, mime_type, data) "
        "VALUES (?, ?, ?, ?, ?, ?)", att->getName(), att->getUploader()->getID(),
        getID(), att->getDate(), att->getMimeType(), data, &q))
    {
        att->setID( q.lastInsertId().toInt() );
        return att;
    }
    delete att;
    return NULL;
}


int Bug::getID() const
{
    return id;
}

const QString & Bug::getTitle() const
{
    return title;
}

const QString & Bug::getDescription() const
{
    return description;
}

const QString & Bug::getModule() const
{
    return module;
}

const QString & Bug::getVersion() const
{
    return version;
}

const QDateTime & Bug::getDate() const
{
    return date;
}

BugStatus Bug::getStatus() const
{
    return status;
}

BugPriority Bug::getPriority() const
{
    return priority;
}

User * Bug::getTester() const
{
    return tester;
}

User * Bug::getDeveloper() const
{
    return developer;
}


// for debugging/testing only
// TODO: remove after it is no longer needed
void Bug::setID(int id)
{
    this->id = id;
}

void Bug::setTitle(const QString &title)
{
    this->title = title;
}

void Bug::setDescription(const QString &description)
{
    this->description = description;
}

void Bug::setModule(const QString &module)
{
    this->module = module;
}

void Bug::setVersion(const QString &version)
{
    this->version = version;
}

void Bug::setStatus(BugStatus status)
{
    this->status = status;
}

void Bug::setPriority(BugPriority priority)
{
    this->priority = priority;
}

void Bug::setTester(int tester_id)
{
    this->tester = User::getByID(tester_id);
}

void Bug::setDeveloper(int dev_id)
{
    this->developer = User::getByID(dev_id);
}
