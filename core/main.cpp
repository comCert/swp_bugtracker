#include <QApplication>
#include <QTranslator>
#include <QSettings>
#include <QDialog>
#include <QMessageBox>
#include <QtGui>

#include "ui/topframe.h"
#include "ui/introwizard.h"
#include "ui/widget/dock.h"
#include "ui/dialog/bugdialog.h"
#include "ui/dialog/optiondialog.h"
#include "ui/widget/fileviewer.h"
#include "util/database.h"

#include "core/user.h"
#include "util/session.h"

int main(int argc, char* argv[])
{
    QApplication app(argc, argv);
    QTranslator translator;
    translator.load("SWT_BugTracker_de"); // loads the german translation file

    /*
     * Standard-Raum (Austausch)
     */
    QSettings conf("BugTracker", QSettings::NativeFormat);

    Topframe frame;
    bool success = false;
    bool introPositive = false;

    frame.show();
    if (conf.value("general/firstStart", true).toBool() == true) {
        IntroWizard intro(&frame);
        intro.setWindowModality(Qt::WindowModal);
        // resize the main frame on first start - niceup
        frame.setMinimumSize(800, 600);
        intro.exec();
        introPositive = intro.result();
    }

    if (!introPositive) {
        if(Session::getDatabase().getConnection().isOpen() == false)
        {
            if(!success && conf.value("user/autoLogin", false).toBool())
                // try login with username and pw from config
                success = Session::login(conf.value("user/loginName").toString(),
                                   conf.value("user/loginPassword").toString());

            if(!success)
                // fall back to guest login
                success = Session::login("guest", "guest");

            if(!success) {
                QMessageBox::critical(0, QObject::tr("Database error"),
                              QObject::tr("Couldn't establish connection to database"),
                              QMessageBox::Cancel, QMessageBox::NoButton);
                return -1;
            }
        }
    } else {
        app.exit(12);
        return 12;
    }

    return app.exec();
}
