#include "attachment.h"
#include "session.h"

#include <QFileInfo>

Attachment::Attachment(const QSqlQuery & q)
{
    id        = q.value(0).toInt();
    uploader  = User::getByID( q.value(1).toInt() );
    date      = q.value(2).toDateTime();
    name      = q.value(3).toString();
    mime_type = q.value(4).toString();
}

Attachment::Attachment(const QString & path)
    : id(-1)
    , uploader(&Session::getUser())
    , date(QDateTime::currentDateTime())
{
    QFileInfo info(path);
    name      = info.fileName();
    mime_type = getMimeTypeBySuffix( info.suffix() );
}

bool Attachment::remove()
{
    return Session::getDatabase().performQuery(
        "DELETE FROM attachment WHERE id=?", getID());
}

QStringList Attachment::formats() const
{
    QStringList sl = QMimeData::formats();
    sl << mime_type;
    return sl;
}

bool Attachment::hasFormat(const QString & mt) const
{
    // ~printf("hasFormat %s\n", mt.toUtf8().data());
    if(mt == "application/x-qt-image" && mime_type.startsWith("image"))
        return true;
    if(mime_type == mt)
        return true;
    return QMimeData::hasFormat(mt);
}

QVariant Attachment::retrieveData(const QString & mt,
                                  QVariant::Type  type) const
{
    if(mime_type == mt ||
        (mt == "application/x-qt-image" && mime_type.startsWith("image")))
    {
        QSqlQuery q;
        if(Session::getDatabase().performQuery(
            "SELECT data FROM attachment WHERE id=?", id, &q))
        {
            q.next();
            // ~if(mime_type.startsWith("image"))
            // ~{
                // ~QPixmap * pm = new QPixmap;
                // ~pm->loadFromData(q.value(0).toByteArray());
                // ~return pm;
            // ~}
            // ~if(mt == "text/plain")
                // ~printf("%s\n", q.value(0).toString().toUtf8().data());
            return q.value(0);
        }
    }
    return QMimeData::retrieveData(mt, type);
}

int Attachment::getID() const
{
    return id;
}

User * Attachment::getUploader() const
{
    return uploader;
}

const QDateTime & Attachment::getDate() const
{
    return date;
}

const QString & Attachment::getName() const
{
    return name;
}

const QString & Attachment::getMimeType() const
{
    return mime_type;
}

void Attachment::setID(int id_)
{
    id = id_;
}

QString Attachment::getMimeTypeBySuffix(const QString & suffix)
{
    // TODO: more types OR find a better method of determining mime type

    QString s = suffix.toLower();
    // images
    if(s == "jpg" || s == "jpeg") return "image/jpeg";
    if(s == "png") return "image/png";
    if(s == "gif") return "image/gif";
    if(s == "svg") return "image/svg+xml";
    // text
    if(s == "htm" || s == "html") return "text/html";
    if(s == "css") return "text/css";
    if(s == "txt" || s == "log") return "text/plain";

    // default
    return "application/octet-stream";
}
