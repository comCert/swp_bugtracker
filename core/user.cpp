#include "user.h"
#include "bug.h"
#include "session.h"
#include "cryptohelper.h"

#include <QSqlQuery>
#include <QVariant>

inline UserType toUserType(int ut)
{
    switch(ut)
    {
    default: // fall through
    case UT_GUEST:           return UT_GUEST;
    case UT_ADMIN:           return UT_ADMIN;
    case UT_DEVELOPER:       return UT_DEVELOPER;
    case UT_CHIEF_DEVELOPER: return UT_CHIEF_DEVELOPER;
    case UT_TESTER:          return UT_TESTER;
    }
}

QString toString(UserType ut)
{
    switch(ut)
    {
    default: // fall through
    case UT_GUEST:           return QObject::tr("Guest");
    case UT_ADMIN:           return QObject::tr("Admin");
    case UT_DEVELOPER:       return QObject::tr("Developer");
    case UT_CHIEF_DEVELOPER: return QObject::tr("Chief Developer");
    case UT_TESTER:          return QObject::tr("Tester");
    }
}

// static members
QSet<QString> User::permissions[UT_LAST];
QMap<int, User> User::users;

// static functions
void User::loadPermissions()
{
    QSqlQuery q;
    if(Session::getDatabase().performQuery(
        "SELECT user_type, perm_name "
        "FROM   permission", &q))
    {
        while(q.next())
            permissions[q.value(0).toInt()].insert(q.value(1).toString());
    }
}

void User::loadMinimalUserInfo()
{
    if(users.contains(-1))
        return;

    QSqlQuery q;
    User user;

    // insert guest user
    user.id       = -1;
    user.username = QObject::tr("nobody");
    user.type     = UT_GUEST;
    users.insert(user.id, user);

    // insert all users from database
    if(Session::getDatabase().performQuery(
        "SELECT id, username FROM user", &q))
    {
        while(q.next())
        {
            user.id       = q.value(0).toInt();
            user.username = q.value(1).toString();
            users.insert(user.id, user);
        }
    }
}

User * User::getByID(int user_id)
{
    QMap<int, User>::iterator it = users.find(user_id);
    if(it == users.end())
        return NULL;
    return &(it.value());
}

User * User::createUser(const QString & username,
                        const QString & password,
                        UserType        type)
{
    QSqlQuery  q;
    /*QByteArray pw;
    QByteArray salt;
    QByteArray hash;

    pw   = password.toAscii();
    salt = CryptoHelper::generateSalt(8);
    pw   = CryptoHelper::applySalt(pw, salt);
    hash = CryptoHelper::generateHash(pw);*/

    if(Session::getDatabase().performQuery(
        "INSERT INTO user (username, pass, type) VALUES (?, ?, ?)",
        username, password, type, &q))
    {
        User user;
        QMap<int, User>::iterator it;

        user.id       = q.lastInsertId().toInt();
        user.username = username;
        it = users.insert(user.id, user);
        return &(it.value());
    }
    return NULL;
}

// public functions
/*bool User::loadById(int user_id)
{
    if(user_id == -1)
    {
        id       = user_id;
        username = QObject::tr("Guest");
        first_name.clear();
        last_name.clear();
        email.clear();
        type     = UT_GUEST;
        return true;
    }
    else
    {
        QSqlQuery q;
        if(Session::getDatabase().performQuery(
            "SELECT username, first_name, last_name, email, type "
            "FROM   user "
            "WHERE  id=?", user_id, &q))
        {
            q.next();
            id         = user_id;
            username   = q.value(0).toString();
            first_name = q.value(1).toString();
            last_name  = q.value(2).toString();
            email      = q.value(3).toString();
            type       = toUserType(q.value(4).toInt());
            return true;
        }
        return false;
    }
}*/

bool User::loadInfo()
{
    QSqlQuery q;
    if(Session::getDatabase().performQuery(
        "SELECT first_name, last_name, email, lastLogin, lastLogout, type "
        "FROM   user "
        "WHERE  id=? ", getID(), &q))
    {
        if(!q.next()) return false;
        first_name = q.value(0).toString();
        last_name  = q.value(1).toString();
        email      = q.value(2).toString();
        lastLogin  = q.value(3).toDateTime();
        lastLogout = q.value(4).toDateTime();
        type       = toUserType(q.value(5).toInt());
        return true;
    }
    return false;
}

bool User::changeInfo(const QString & first_name,
                      const QString & last_name,
                      const QString & email)
{
    if(Session::getDatabase().performQuery(
        "UPDATE user "
        "SET    first_name=?, last_name=?, email=? "
        "WHERE  id=?", first_name, last_name, email, getID()))
    {
        this->first_name = first_name;
        this->last_name  = last_name;
        this->email      = email;
        return true;
    }
    return false;
}

bool User::changeType(UserType type)
{
    if(Session::getDatabase().performQuery(
        "UPDATE user SET type=? WHERE id=?", type, getID()))
    {
        this->type = type;
        return true;
    }
    return false;
}

void User::changePassword(const QString & password,
                          const QString & old_password)
{
    //TODO
}

bool User::hasPermission(const QString & perm) const
{
    // debug
    //QSet<QString> & m = permissions[type];
    //QSet<QString>::iterator i = m.begin(), e = m.end();
    //for(; i != e; ++i)
     //   printf("%s\n", i->toUtf8().data());
    return permissions[type].contains(perm);
}

QList<Bug *> User::getBugs() const
{
    QSqlQuery    q;
    QList<Bug *> l;

    if(Session::getDatabase().performQuery(
        "SELECT   id, title, description, module, version, date,"
        "         state, priority, tester_id, developer_id "
        "FROM     bug "
        "WHERE    tester_id=? OR developer_id=? "
        "ORDER BY date DESC "
        "LIMIT    5", getID(), getID(), &q))
    {
        while(q.next())
            l.append(new Bug(q));
    }
    return l;
}

bool User::reportBug(Bug *bug)
{
    QSqlQuery q;
    q.prepare("INSERT INTO bug (title, description, module, version, date,"
              "state, priority, tester_id, developer_id) VALUES "
              "(?, ?, ?, ?, ?, ?, ?, ?, ?)");
    q.addBindValue(bug->getTitle());
    q.addBindValue(bug->getDescription());
    q.addBindValue(bug->getModule());
    q.addBindValue(bug->getVersion());
    q.addBindValue(bug->getDate());
    q.addBindValue(bug->getStatus());
    q.addBindValue(bug->getPriority());
    q.addBindValue(getID());
    q.addBindValue(-1);
    if(q.exec())
    {
        bug->setID( q.lastInsertId().toInt() );
        return true;
    }
    return false;
}

bool User::assignBug(Bug *bug)
{
    if(Session::getDatabase().performQuery(
        "UPDATE bug "
        "SET    developer_id=? "
        "WHERE  id=?", getID(), bug->getID()))
    {
        bug->setDeveloper(getID());
        return true;
    }
    return false;
}

// getter
int User::getID() const
{
    return id;
}

const QString & User::getUsername() const
{
    return username;
}

const QString & User::getFirstName() const
{
    return first_name;
}

const QString & User::getLastName() const
{
    return last_name;
}

const QString & User::getEmail() const
{
    return email;
}

const QDateTime & User::getLastLogin () const
{
    return lastLogin;
}

const QDateTime & User::getLastLogout() const
{
    return lastLogout;
}

UserType User::getType() const
{
    return type;
}
