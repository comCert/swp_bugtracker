#ifndef ATTACHMENT_H
#define ATTACHMENT_H

#include <QString>
#include <QDateTime>
#include <QSqlQuery>
#include <QMimeData>
#include <QVariant>
#include <QStringList>

class User;

class Attachment : public QMimeData
{
    Q_OBJECT

public:

    Attachment(const QSqlQuery & q);
    Attachment(const QString & path);

    bool remove();

    virtual QStringList formats() const;
    virtual bool        hasFormat(const QString & mimeType) const;

    int               getID()       const;
    User *            getUploader() const;
    const QDateTime & getDate()     const;
    const QString &   getName()     const;
    const QString &   getMimeType() const;

    void setID(int id);

    static QString getMimeTypeBySuffix(const QString & suffix);

protected:

    virtual QVariant retrieveData(const QString & mime_type,
                                  QVariant::Type  type) const;

private:

    int id;
    User * uploader;
    QDateTime date;
    QString name;
    QString mime_type;

};

#endif /* ATTACHMENT_H */
