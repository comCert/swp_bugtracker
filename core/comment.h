#ifndef COMMENT_H
#define COMMENT_H

#include <QSqlQuery>
#include <QString>
#include <QDateTime>

class User;
class Bug;

class Comment
{
public:

    Comment(const QSqlQuery & q, const int map[] = NULL);
    Comment(const QString & text);

    bool edit(const QString & new_text);
    bool remove();

    int               getID()     const;
    User            * getAuthor() const;
    //Bug             * getBug()    const;
    const QDateTime & getDate()   const;
    const QString   & getText()   const;

    void setID(int id);

private:

    int         id;
    User      * author;
    //Bug       * bug;
    QDateTime   date;
    QString     text;

};

#endif /* COMMENT_H */
