#!/bin/bash
# updateTranslatorFiles <localeToEdit>

if [ $# -ne 1 ]; then
	echo 1>&2 Usage: $0 localeToEdit
	exit 127
fi

lupdate SWT_BugTracker.pro
linguist res/ts/SWT_BugTracker_$1.ts
