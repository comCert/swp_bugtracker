CONFIG += \
    qt

QT += \
    sql \
    gui

HEADERS += \
    ui/topframe.h \
    core/bug.h \
    core/user.h \
    core/comment.h \
    core/attachment.h \
    util/session.h \
    util/database.h \
    util/querybuilder.h \
    util/bugtablemodel.h \
    util/attachmenttablemodel.h \
    util/cryptohelper.h \
    ui/introwizard.h \
    ui/widget/dock.h \
    ui/widget/bugoverview.h \
    ui/widget/buglist.h \
    ui/widget/buginfo.h \
    ui/dialog/bugdialog.h \
    util/dtime.h \
    ui/dialog/optiondialog.h \
    util/helpbrowser.h \
    ui/widget/searchbox.h \
    ui/widget/fileviewer.h \
    ui/dialog/logindialog.h \
    ui/dialog/userdialog.h \
    ui/widget/clickablelabel.h

SOURCES += \
    ui/topframe.cpp \
    core/main.cpp \
    core/bug.cpp \
    core/user.cpp \
    core/comment.cpp \
    core/attachment.cpp \
    util/session.cpp \
    util/database.cpp \
    util/querybuilder.cpp \
    util/bugtablemodel.cpp \
    util/attachmenttablemodel.cpp \
    util/cryptohelper.cpp \
    ui/introwizard.cpp \
    ui/widget/dock.cpp \
    ui/widget/bugoverview.cpp \
    ui/widget/buglist.cpp \
    ui/widget/buginfo.cpp \
    ui/widget/searchbox.cpp \
    ui/dialog/bugdialog.cpp \
    util/dtime.cpp \
    ui/dialog/optiondialog.cpp \
    util/helpbrowser.cpp \
    ui/widget/fileviewer.cpp \
    ui/dialog/logindialog.cpp \
    ui/dialog/userdialog.cpp \
    ui/widget/clickablelabel.cpp

TRANSLATIONS += \
    res/ts/SWT_BugTracker_de.ts

INCLUDEPATH += \
    ui \
    core \
    util

#DESTDIR = \
#    debug { \
#        bin/debug/ \
#    } \
#    release { \
#        bin/release/ \
#    }

#wince*: DEPLOYMENT_PLUGIN += \
#    qsqlite

OTHER_FILES += \
    README \
    doc/ToDo.txt \
    doc/News.txt \
    doc/FixMe.txt \
    doc/Config.txt \
    doc/LICENSE.txt

RESOURCES += \
    res.qrc
