<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de_DE">
<context>
    <name>AttachmentTableModel</name>
    <message>
        <location filename="../../util/attachmenttablemodel.cpp" line="85"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../../util/attachmenttablemodel.cpp" line="86"/>
        <source>Filename</source>
        <translation>Dateiname</translation>
    </message>
    <message>
        <location filename="../../util/attachmenttablemodel.cpp" line="87"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="../../util/attachmenttablemodel.cpp" line="88"/>
        <source>Mime Type</source>
        <translation>Mime-Typ</translation>
    </message>
    <message>
        <location filename="../../util/attachmenttablemodel.cpp" line="89"/>
        <source>Uploader</source>
        <translation>Uploader</translation>
    </message>
</context>
<context>
    <name>BugDialog</name>
    <message>
        <location filename="../../ui/dialog/bugdialog.cpp" line="12"/>
        <source>Title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <location filename="../../ui/dialog/bugdialog.cpp" line="14"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location filename="../../ui/dialog/bugdialog.cpp" line="17"/>
        <source>0/200</source>
        <translation>0/200</translation>
    </message>
    <message>
        <location filename="../../ui/dialog/bugdialog.cpp" line="21"/>
        <source>Module</source>
        <translation>Modul</translation>
    </message>
    <message>
        <location filename="../../ui/dialog/bugdialog.cpp" line="23"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../../ui/dialog/bugdialog.cpp" line="25"/>
        <source>Priority</source>
        <translation>Priorität</translation>
    </message>
    <message>
        <location filename="../../ui/dialog/bugdialog.cpp" line="59"/>
        <source>Submit an untracked bug</source>
        <translation>Einen neuen Bug melden</translation>
    </message>
    <message>
        <location filename="../../ui/dialog/bugdialog.cpp" line="83"/>
        <source>Warning: Losing information</source>
        <translation>Warnung: Informationen gehen verloren</translation>
    </message>
    <message>
        <location filename="../../ui/dialog/bugdialog.cpp" line="99"/>
        <source>This is the form wherein you can offer information according to a new bug you spotted.Please keep your report short and informativ.


If there are unusable elements then you haven&apos;t the rights to fill this informations.

Please aquire the documentation for more help.</source>
        <translation>Das ist das Formular, wo man Informationen zu einem neuen Bug melden kann. Bitte halten Sie Ihre Meldung kurz und sachlich.


Wenn nicht nutzbare Elemente im Formular erscheinen, dann verfügen Sie nicht über die nötigen Rechte dafür.

Bitte ersuchen Sie die Dokumentation für mehr Informationen.</translation>
    </message>
    <message>
        <source>low</source>
        <translation type="obsolete">niedrig</translation>
    </message>
    <message>
        <source>default</source>
        <translation type="obsolete">standard</translation>
    </message>
    <message>
        <source>high</source>
        <translation type="obsolete">hoch</translation>
    </message>
    <message>
        <source>urgent</source>
        <translation type="obsolete">sehr hoch</translation>
    </message>
    <message>
        <source>State</source>
        <translation type="obsolete">Status</translation>
    </message>
    <message>
        <source>not assigned</source>
        <translation type="obsolete">nicht zugewiesen</translation>
    </message>
    <message>
        <source>assigned</source>
        <translation type="obsolete">zugewiesen</translation>
    </message>
    <message>
        <source>fixing</source>
        <translation type="obsolete">unter Bearbeitung</translation>
    </message>
    <message>
        <source>fixed</source>
        <translation type="obsolete">ausgebessert</translation>
    </message>
    <message>
        <source>closed</source>
        <translation type="obsolete">geschlossen</translation>
    </message>
    <message>
        <source>Date</source>
        <translation type="obsolete">Datum</translation>
    </message>
    <message>
        <source>Developer</source>
        <translation type="obsolete">Entwickler</translation>
    </message>
    <message>
        <source>Tester</source>
        <translation type="obsolete">Tester</translation>
    </message>
    <message>
        <source>Warning: Loosing information</source>
        <translation type="obsolete">Warnung: Informationsverlust</translation>
    </message>
    <message>
        <location filename="../../ui/dialog/bugdialog.cpp" line="84"/>
        <source>If you continue, all your entered information will be lost!</source>
        <translation>Wenn Sie fortfahren, gehen alle Informationen, die Sie eingegeben haben, verloren!</translation>
    </message>
    <message>
        <location filename="../../ui/dialog/bugdialog.cpp" line="98"/>
        <source>Help: Bug-Form</source>
        <translation>Hilfe: Bug-Formular</translation>
    </message>
    <message>
        <source>some help text here: TODO</source>
        <translation type="obsolete">etwas Hilfe-Text hier: TODO</translation>
    </message>
    <message>
        <source>Transaction</source>
        <translation type="obsolete">Transaktion</translation>
    </message>
    <message>
        <source>Your transaction to the database was successfull!</source>
        <translation type="obsolete">Ihre Transaktion zur Datenbank war erfolgreich!</translation>
    </message>
</context>
<context>
    <name>BugInfo</name>
    <message>
        <source>Date</source>
        <translation type="obsolete">Datum</translation>
    </message>
    <message>
        <source>Version</source>
        <translation type="obsolete">Version</translation>
    </message>
    <message>
        <source>Module</source>
        <translation type="obsolete">Modul</translation>
    </message>
    <message>
        <source>Status</source>
        <translation type="obsolete">Status</translation>
    </message>
    <message>
        <source>Priority</source>
        <translation type="obsolete">Priorität</translation>
    </message>
    <message>
        <location filename="../../ui/widget/buginfo.cpp" line="21"/>
        <source>Info</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../../ui/widget/buginfo.cpp" line="29"/>
        <source>Title:</source>
        <translation>Titel:</translation>
    </message>
    <message>
        <location filename="../../ui/widget/buginfo.cpp" line="33"/>
        <source>Date:</source>
        <translation>Datum:</translation>
    </message>
    <message>
        <location filename="../../ui/widget/buginfo.cpp" line="37"/>
        <source>Version:</source>
        <translation>Version:</translation>
    </message>
    <message>
        <location filename="../../ui/widget/buginfo.cpp" line="41"/>
        <source>Module:</source>
        <translation>Modul:</translation>
    </message>
    <message>
        <location filename="../../ui/widget/buginfo.cpp" line="46"/>
        <source>Grab</source>
        <translation>Übernehmen</translation>
    </message>
    <message>
        <location filename="../../ui/widget/buginfo.cpp" line="47"/>
        <source>Accept</source>
        <translation>Akzeptieren</translation>
    </message>
    <message>
        <location filename="../../ui/widget/buginfo.cpp" line="48"/>
        <source>Done</source>
        <translation>Erledigt</translation>
    </message>
    <message>
        <location filename="../../ui/widget/buginfo.cpp" line="49"/>
        <source>Confirm</source>
        <translation>Bestätigen</translation>
    </message>
    <message>
        <location filename="../../ui/widget/buginfo.cpp" line="50"/>
        <source>Reject</source>
        <translation>Abweisen</translation>
    </message>
    <message>
        <location filename="../../ui/widget/buginfo.cpp" line="68"/>
        <source>Status:</source>
        <translation>Status:</translation>
    </message>
    <message>
        <location filename="../../ui/widget/buginfo.cpp" line="82"/>
        <location filename="../../ui/widget/buginfo.cpp" line="89"/>
        <source>Priority:</source>
        <translation>Priorität:</translation>
    </message>
    <message>
        <location filename="../../ui/widget/buginfo.cpp" line="94"/>
        <source>Tester:</source>
        <translation>Tester:</translation>
    </message>
    <message>
        <location filename="../../ui/widget/buginfo.cpp" line="104"/>
        <source>Assign</source>
        <translation>Zuweisen</translation>
    </message>
    <message>
        <location filename="../../ui/widget/buginfo.cpp" line="123"/>
        <location filename="../../ui/widget/buginfo.cpp" line="128"/>
        <source>Developer:</source>
        <translation>Entwickler:</translation>
    </message>
    <message>
        <location filename="../../ui/widget/buginfo.cpp" line="136"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location filename="../../ui/widget/buginfo.cpp" line="145"/>
        <source>Attachments</source>
        <translation>Anhänge</translation>
    </message>
    <message>
        <location filename="../../ui/widget/buginfo.cpp" line="168"/>
        <source>Browse</source>
        <translation>Durchsuchen</translation>
    </message>
    <message>
        <location filename="../../ui/widget/buginfo.cpp" line="172"/>
        <source>Upload</source>
        <translation>Hochladen</translation>
    </message>
    <message>
        <location filename="../../ui/widget/buginfo.cpp" line="184"/>
        <source>Comments</source>
        <translation>Kommentare</translation>
    </message>
    <message>
        <location filename="../../ui/widget/buginfo.cpp" line="192"/>
        <source>Submit</source>
        <translation>Einpflegen</translation>
    </message>
    <message>
        <location filename="../../ui/widget/buginfo.cpp" line="203"/>
        <source>no comments</source>
        <translation>keine Kommentare</translation>
    </message>
    <message>
        <location filename="../../ui/widget/buginfo.cpp" line="235"/>
        <source>&lt;i&gt;%1&lt;/i&gt; wrote on %2:</source>
        <translation>&lt;i&gt;%1&lt;/i&gt; veröffentlicht am %2:</translation>
    </message>
    <message>
        <location filename="../../ui/widget/buginfo.cpp" line="252"/>
        <source>Select File</source>
        <translation>Datei wählen</translation>
    </message>
</context>
<context>
    <name>BugList</name>
    <message>
        <location filename="../../ui/widget/buglist.cpp" line="17"/>
        <source>&amp;Refresh</source>
        <translation>&amp;Aktualisieren</translation>
    </message>
    <message>
        <location filename="../../ui/widget/buglist.cpp" line="43"/>
        <source>Filter ...</source>
        <translation>Filter ...</translation>
    </message>
</context>
<context>
    <name>BugOverview</name>
    <message>
        <location filename="../../ui/widget/bugoverview.cpp" line="11"/>
        <source>&lt;strong&gt;Recent Bugs&lt;/strong&gt;</source>
        <translation>&lt;strong&gt;Aktuelle Bugs&lt;/strong&gt;</translation>
    </message>
    <message>
        <location filename="../../ui/widget/bugoverview.cpp" line="12"/>
        <source>&lt;strong&gt;Recent Comments&lt;/strong&gt;</source>
        <translation>&lt;strong&gt;Aktuelle Kommentare&lt;/strong&gt;</translation>
    </message>
    <message>
        <location filename="../../ui/widget/bugoverview.cpp" line="13"/>
        <source>Refresh</source>
        <translation>Aktualisieren</translation>
    </message>
</context>
<context>
    <name>BugTableModel</name>
    <message>
        <location filename="../../util/bugtablemodel.cpp" line="6"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../../util/bugtablemodel.cpp" line="7"/>
        <source>Title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <location filename="../../util/bugtablemodel.cpp" line="8"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location filename="../../util/bugtablemodel.cpp" line="9"/>
        <source>Module</source>
        <translation>Modul</translation>
    </message>
    <message>
        <location filename="../../util/bugtablemodel.cpp" line="10"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../../util/bugtablemodel.cpp" line="11"/>
        <source>Priority</source>
        <translation>Priorität</translation>
    </message>
    <message>
        <location filename="../../util/bugtablemodel.cpp" line="12"/>
        <source>State</source>
        <translation>Status</translation>
    </message>
    <message>
        <source>Status</source>
        <translation type="obsolete">Status</translation>
    </message>
    <message>
        <location filename="../../util/bugtablemodel.cpp" line="13"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="../../util/bugtablemodel.cpp" line="14"/>
        <source>Developer</source>
        <translation>Entwickler</translation>
    </message>
    <message>
        <location filename="../../util/bugtablemodel.cpp" line="15"/>
        <source>Tester</source>
        <translation>Tester</translation>
    </message>
</context>
<context>
    <name>DatabasePage</name>
    <message>
        <source>Connection information</source>
        <translation type="obsolete">Verbindungsinformation</translation>
    </message>
    <message>
        <source>Connection information into the database</source>
        <translation type="obsolete">Verbindungsinformation zur Datenbank</translation>
    </message>
    <message>
        <source>hostname or ip-address</source>
        <translation type="obsolete">Hostname oder IP-Adresse</translation>
    </message>
    <message>
        <source>port</source>
        <translation type="obsolete">Port</translation>
    </message>
    <message>
        <source>database name</source>
        <translation type="obsolete">Datenbankname</translation>
    </message>
</context>
<context>
    <name>Dock</name>
    <message>
        <source>DockIt</source>
        <translation type="obsolete">DockIt</translation>
    </message>
    <message>
        <location filename="../../ui/widget/dock.cpp" line="13"/>
        <source>Overview</source>
        <translation>Übersicht</translation>
    </message>
    <message>
        <location filename="../../ui/widget/dock.cpp" line="14"/>
        <source>Your personal overview of current happenings</source>
        <translation>Ihre persönliche Übersicht über aktuelle Ereignisse</translation>
    </message>
    <message>
        <location filename="../../ui/widget/dock.cpp" line="16"/>
        <source>List</source>
        <translation>Liste</translation>
    </message>
    <message>
        <location filename="../../ui/widget/dock.cpp" line="17"/>
        <source>List of all bugs</source>
        <translation>Liste aller Bugs</translation>
    </message>
    <message>
        <location filename="../../ui/widget/dock.cpp" line="19"/>
        <source>Commit Bug</source>
        <translation>Bug melden</translation>
    </message>
    <message>
        <location filename="../../ui/widget/dock.cpp" line="21"/>
        <source>Opens a form to create a new bug entry</source>
        <translation>Öffnet das Formular für einen neuen Bug-Eintrag</translation>
    </message>
    <message>
        <location filename="../../ui/widget/dock.cpp" line="24"/>
        <location filename="../../ui/widget/dock.cpp" line="51"/>
        <source>Login</source>
        <translation>Login</translation>
    </message>
    <message>
        <location filename="../../ui/widget/dock.cpp" line="53"/>
        <source>Logout</source>
        <translation>Logout</translation>
    </message>
    <message>
        <location filename="../../ui/widget/dock.cpp" line="101"/>
        <location filename="../../ui/widget/dock.cpp" line="102"/>
        <source>Login failed</source>
        <translation>Anmeldung fehlgeschlagen</translation>
    </message>
    <message>
        <source>Buglist</source>
        <translation type="obsolete">Bug-Liste</translation>
    </message>
</context>
<context>
    <name>ExitPage</name>
    <message>
        <source>Conclusion</source>
        <translation type="obsolete">Zusammenfassung</translation>
    </message>
    <message>
        <source>Please check your submitted data before you finish</source>
        <translation type="obsolete">Bitte überprüfen Sie Ihre eingegebenen Daten bevor Sie die Eingabe beenden</translation>
    </message>
    <message>
        <source>Your name:	</source>
        <translation type="obsolete">Ihr Name:</translation>
    </message>
    <message>
        <source>Login name:	</source>
        <translation type="obsolete">Login-Name:</translation>
    </message>
    <message>
        <source>password:	</source>
        <translation type="obsolete">Passwort:</translation>
    </message>
    <message>
        <source>hostname:	</source>
        <translation type="obsolete">Hostname:</translation>
    </message>
    <message>
        <source>port:	</source>
        <translation type="obsolete">Port:</translation>
    </message>
    <message>
        <source>database name:	</source>
        <translation type="obsolete">Datenbankname:</translation>
    </message>
</context>
<context>
    <name>FileViewer</name>
    <message>
        <location filename="../../ui/widget/fileviewer.cpp" line="15"/>
        <source>by </source>
        <translation>von</translation>
    </message>
    <message>
        <location filename="../../ui/widget/fileviewer.cpp" line="23"/>
        <source>Download</source>
        <translation>Download</translation>
    </message>
    <message>
        <location filename="../../ui/widget/fileviewer.cpp" line="56"/>
        <source>Save as</source>
        <translation>Speichern als</translation>
    </message>
    <message>
        <location filename="../../ui/widget/fileviewer.cpp" line="59"/>
        <source>Error while saving</source>
        <translation>Fehler beim Speichern</translation>
    </message>
    <message>
        <location filename="../../ui/widget/fileviewer.cpp" line="60"/>
        <source>There was an error! File was not saved!</source>
        <translation>Ein Fehler trat auf! Datei wurde nicht gespeichert!</translation>
    </message>
    <message>
        <location filename="../../ui/widget/fileviewer.cpp" line="67"/>
        <source>Error saving</source>
        <translation>Fehler beim Speichern</translation>
    </message>
    <message>
        <location filename="../../ui/widget/fileviewer.cpp" line="68"/>
        <source>Cannot write file!</source>
        <translation>Datei schreiben nicht möglich!</translation>
    </message>
</context>
<context>
    <name>IntroPage</name>
    <message>
        <source>Introduction</source>
        <translation type="obsolete">Einleitung</translation>
    </message>
    <message>
        <source>This wizard will guide you through the configuration of the bug tracking software you use the first time.During this initial process you require some login information, which you should have get from your administrator.


If you don&apos;t have any information described above, cancel and ask for it in your company.</source>
        <translation type="obsolete">Dieser Assistent wird Sie durch die Einrichtung dieser Bug-Behgebungs Software, welche Sie zum ersten Mal benutzen, führen. Während diesem Einleitungsprozess werden Sie Login-Informationen benötigen, die Sie von Ihrem Administrator erhalten haben sollten.


Falls Sie diese Informationen nicht zur Hand haben, beenden Sie bitte den Prozess und fragen Sie in Ihrem Unternehmen danach.</translation>
    </message>
</context>
<context>
    <name>IntroWizard</name>
    <message>
        <location filename="../../ui/introwizard.cpp" line="28"/>
        <source>Intro Wizard</source>
        <translation>Eirichtungs-Assistent</translation>
    </message>
    <message>
        <location filename="../../ui/introwizard.cpp" line="153"/>
        <source>Introduction</source>
        <translation>Einleitung</translation>
    </message>
    <message>
        <source>This wizard will guide you through the configuration of the bug tracking software you use the first time.During this initial process you require some login information, which you should have get from your administrator.


If you don&apos;t have any information described above, cancel and ask for it in your company.</source>
        <translation type="obsolete">Dieser Assistent wird Sie durch die Einrichtung dieser Bug-Behebungs Software, welche Sie zum ersten Mal benutzen, führen. Während diesem Einleitungsprozess werden Sie Login-Informationen benötigen, die Sie von Ihrem Administrator erhalten haben sollten.


Falls Sie diese Informationen nicht zur Hand haben, beenden Sie bitte den Prozess und fragen Sie in Ihrem Unternehmen danach.</translation>
    </message>
    <message>
        <location filename="../../ui/introwizard.cpp" line="73"/>
        <source>&amp;Quit</source>
        <translation>&amp;Beenden</translation>
    </message>
    <message>
        <location filename="../../ui/introwizard.cpp" line="74"/>
        <source>Warning: Loss of Information</source>
        <translation>Warnung: Verlust von Informationen</translation>
    </message>
    <message>
        <location filename="../../ui/introwizard.cpp" line="76"/>
        <source>All given information will be lost if continuing!</source>
        <translation>Alle angegebenen Informationen werden verworfen, wenn Sie fortfahren!</translation>
    </message>
    <message>
        <location filename="../../ui/introwizard.cpp" line="106"/>
        <location filename="../../ui/introwizard.cpp" line="107"/>
        <location filename="../../ui/introwizard.cpp" line="108"/>
        <source>guest</source>
        <translation>Gast</translation>
    </message>
    <message>
        <location filename="../../ui/introwizard.cpp" line="110"/>
        <source>Deaktivate Guest Login</source>
        <translation>Deaktiviere Gast-Login</translation>
    </message>
    <message>
        <location filename="../../ui/introwizard.cpp" line="117"/>
        <location filename="../../ui/introwizard.cpp" line="187"/>
        <source>Guest Login</source>
        <translation>Gast-Login</translation>
    </message>
    <message>
        <location filename="../../ui/introwizard.cpp" line="127"/>
        <source>checking login - please be patient ...</source>
        <translation>Anmeldung - bitte warten ...</translation>
    </message>
    <message>
        <location filename="../../ui/introwizard.cpp" line="136"/>
        <source>Success! Please confirm.</source>
        <translation>Erfolg! Bitte fahren Sie fort.</translation>
    </message>
    <message>
        <location filename="../../ui/introwizard.cpp" line="140"/>
        <source>Error! Please check information.</source>
        <translation>Fehler! Bitte prüfen Sie Ihre Informationen.</translation>
    </message>
    <message>
        <location filename="../../ui/introwizard.cpp" line="154"/>
        <source>This wizard will guide you through the configuration of the bug tracking software you use the first time. During this initial process you require some login information, which you should have get from your administrator.


If you don&apos;t have any information described above, cancel and ask for it in your company.</source>
        <translation>Dieser Assistent wird Ihnen dabei helfen, Ihre Bug-Tracking-Software einzurichten. Im Verlauf dieses Vorgangs werden Sie einige Anmeldeinformationen benötigen, die Sie von Ihrem Administrator erhalten haben sollten.


Falls Sie diese Informationen nicht haben, beenden Sie den Assistenten und fragen Sie in Ihrer Firma danach.</translation>
    </message>
    <message>
        <location filename="../../ui/introwizard.cpp" line="167"/>
        <source>User information</source>
        <translation>Nutzerinformation</translation>
    </message>
    <message>
        <location filename="../../ui/introwizard.cpp" line="168"/>
        <source>Login ingormation into the database</source>
        <translation>Login-Informationen in die Datenbank</translation>
    </message>
    <message>
        <location filename="../../ui/introwizard.cpp" line="172"/>
        <source>Your name</source>
        <translation>Ihr Name</translation>
    </message>
    <message>
        <location filename="../../ui/introwizard.cpp" line="174"/>
        <source>Login name</source>
        <translation>Login-Name</translation>
    </message>
    <message>
        <location filename="../../ui/introwizard.cpp" line="176"/>
        <source>Password</source>
        <translation>Passwort</translation>
    </message>
    <message>
        <location filename="../../ui/introwizard.cpp" line="178"/>
        <location filename="../../ui/introwizard.cpp" line="183"/>
        <source>hidden</source>
        <translation>verborgen</translation>
    </message>
    <message>
        <location filename="../../ui/introwizard.cpp" line="181"/>
        <source>Confirm password</source>
        <translation>Passwort bestätigen</translation>
    </message>
    <message>
        <location filename="../../ui/introwizard.cpp" line="203"/>
        <source>Connection information</source>
        <translation>Verbindungsinformation</translation>
    </message>
    <message>
        <location filename="../../ui/introwizard.cpp" line="204"/>
        <source>Connection information into the database</source>
        <translation>Verbindungsinformation zur Datenbank</translation>
    </message>
    <message>
        <location filename="../../ui/introwizard.cpp" line="209"/>
        <source>Hostname or ip-address</source>
        <translation>Hostname oder IP-Adresse</translation>
    </message>
    <message>
        <location filename="../../ui/introwizard.cpp" line="211"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location filename="../../ui/introwizard.cpp" line="214"/>
        <source>Database name</source>
        <translation>Datenbankname</translation>
    </message>
    <message>
        <location filename="../../ui/introwizard.cpp" line="216"/>
        <source>Check Login</source>
        <translation>Prüfe Login</translation>
    </message>
    <message>
        <location filename="../../ui/introwizard.cpp" line="217"/>
        <source>Check the given login information and try to connect to the database.</source>
        <translation>Prüfe die angegebenen Informationen und versuche zur Datenbank zu verbinden.</translation>
    </message>
    <message>
        <location filename="../../ui/introwizard.cpp" line="234"/>
        <source>Conclusion</source>
        <translation>Zusammenfassung</translation>
    </message>
    <message>
        <location filename="../../ui/introwizard.cpp" line="235"/>
        <source>Please check your submitted data before you finish</source>
        <translation>Bitte überprüfen Sie Ihre eingegebenen Daten, bevor Sie die Eingabe beenden</translation>
    </message>
    <message>
        <location filename="../../ui/introwizard.cpp" line="250"/>
        <source>Your name:		</source>
        <translation>Ihre Name:</translation>
    </message>
    <message>
        <location filename="../../ui/introwizard.cpp" line="253"/>
        <source>Login name:		</source>
        <translation>Ihre Login-Name:</translation>
    </message>
    <message>
        <location filename="../../ui/introwizard.cpp" line="256"/>
        <source>password:		</source>
        <translation>Passwort:</translation>
    </message>
    <message>
        <source>Your name:	</source>
        <translation type="obsolete">Ihr Name:</translation>
    </message>
    <message>
        <source>Login name:	</source>
        <translation type="obsolete">Login-Name:</translation>
    </message>
    <message>
        <source>password:	</source>
        <translation type="obsolete">Passwort:</translation>
    </message>
    <message>
        <location filename="../../ui/introwizard.cpp" line="264"/>
        <source>hostname:		</source>
        <translation>Hostname:</translation>
    </message>
    <message>
        <location filename="../../ui/introwizard.cpp" line="267"/>
        <source>port:		</source>
        <translation>Port:</translation>
    </message>
    <message>
        <location filename="../../ui/introwizard.cpp" line="270"/>
        <source>database name:	</source>
        <translation>Datenbankname:</translation>
    </message>
    <message>
        <location filename="../../ui/introwizard.cpp" line="287"/>
        <source>The given information might be not valid!</source>
        <translation>Die angegebenen Informationen könnten fehlerhaft sein!</translation>
    </message>
    <message>
        <location filename="../../ui/introwizard.cpp" line="290"/>
        <source>The given information might be valid!</source>
        <translation>Die angegebenen Informationen könnten stimmen!</translation>
    </message>
    <message>
        <location filename="../../ui/introwizard.cpp" line="303"/>
        <source>empty password not accepted</source>
        <translation>leeres Passwort wird nicht akzeptiert</translation>
    </message>
    <message>
        <location filename="../../ui/introwizard.cpp" line="306"/>
        <source>passwords match</source>
        <translation>Passwörter stimmen überein</translation>
    </message>
    <message>
        <location filename="../../ui/introwizard.cpp" line="309"/>
        <source>passwords don&apos;t match</source>
        <translation>Passwörter stimmen nicht überein</translation>
    </message>
</context>
<context>
    <name>LoginDialog</name>
    <message>
        <location filename="../../ui/dialog/logindialog.cpp" line="20"/>
        <source>Login</source>
        <translation>Anmeldung</translation>
    </message>
</context>
<context>
    <name>OptionDialog</name>
    <message>
        <location filename="../../ui/dialog/optiondialog.cpp" line="75"/>
        <source>Confirm closing the main window</source>
        <translation>Bestätigen Sie das Beenden des Programms</translation>
    </message>
    <message>
        <location filename="../../ui/dialog/optiondialog.cpp" line="76"/>
        <source>If this option is checked, there will popup a small dialog, whichasks you, if you are shure to close the application.</source>
        <translation>Falls diese Option angewählt ist, dann wird beim Beenden des Programms ein kleines Fenster erscheinen und Sie um Bestätigung des Aktion bitten.</translation>
    </message>
    <message>
        <location filename="../../ui/dialog/optiondialog.cpp" line="78"/>
        <source>Show dock in the main window</source>
        <translation>Zeige das Dock im Hauptfenster</translation>
    </message>
    <message>
        <location filename="../../ui/dialog/optiondialog.cpp" line="79"/>
        <source>If this option is checked, the dock in the main window will be shown.It offers you a quick access to some important functions to improveyour workflow.</source>
        <translation>Falls die Option angewählt ist, dann wird das Dock im Hauptfenster angezeigt. Es bietet schnellen Zugriff auf wichtige Informationen und verbessert so Ihren Arbeitsfluss.</translation>
    </message>
    <message>
        <location filename="../../ui/dialog/optiondialog.cpp" line="95"/>
        <source>Your name</source>
        <translation>Ihr Name</translation>
    </message>
    <message>
        <location filename="../../ui/dialog/optiondialog.cpp" line="96"/>
        <source>Your login name</source>
        <translation>Ihr Login-Name</translation>
    </message>
    <message>
        <location filename="../../ui/dialog/optiondialog.cpp" line="97"/>
        <source>Password</source>
        <translation>Passwort</translation>
    </message>
    <message>
        <location filename="../../ui/dialog/optiondialog.cpp" line="107"/>
        <source>Hostname or IP-Address</source>
        <translation>Hostname oder IP-Adresse</translation>
    </message>
    <message>
        <location filename="../../ui/dialog/optiondialog.cpp" line="108"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location filename="../../ui/dialog/optiondialog.cpp" line="109"/>
        <source>Database name</source>
        <translation>Datenbankname</translation>
    </message>
    <message>
        <location filename="../../ui/dialog/optiondialog.cpp" line="112"/>
        <source>General</source>
        <translation>Allgemein</translation>
    </message>
    <message>
        <location filename="../../ui/dialog/optiondialog.cpp" line="113"/>
        <source>General settings of the application</source>
        <translation>Allgemeine Einstellungen des Programms</translation>
    </message>
    <message>
        <location filename="../../ui/dialog/optiondialog.cpp" line="115"/>
        <source>User</source>
        <translation>Nutzer</translation>
    </message>
    <message>
        <location filename="../../ui/dialog/optiondialog.cpp" line="116"/>
        <source>User information</source>
        <translation>Nutzerinformation</translation>
    </message>
    <message>
        <location filename="../../ui/dialog/optiondialog.cpp" line="118"/>
        <source>Connection</source>
        <translation>Verbindung</translation>
    </message>
    <message>
        <location filename="../../ui/dialog/optiondialog.cpp" line="119"/>
        <source>Connection information</source>
        <translation>Verbindungsinformation</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../core/main.cpp" line="53"/>
        <location filename="../../util/database.cpp" line="71"/>
        <source>Database error</source>
        <translation>Datenbank-Fehler</translation>
    </message>
    <message>
        <location filename="../../core/main.cpp" line="54"/>
        <source>Couldn&apos;t establish connection to database</source>
        <translation>Konnte keine Verbindung zur Datenbank herstellen</translation>
    </message>
    <message>
        <source>There is a database error occured:

</source>
        <translation type="obsolete">Es trat ein Datenbank-Fehler auf:</translation>
    </message>
    <message>
        <location filename="../../core/bug.cpp" line="40"/>
        <source>low</source>
        <translation>niedrig</translation>
    </message>
    <message>
        <location filename="../../core/bug.cpp" line="42"/>
        <source>default</source>
        <translation>standard</translation>
    </message>
    <message>
        <location filename="../../core/bug.cpp" line="43"/>
        <source>high</source>
        <translation>hoch</translation>
    </message>
    <message>
        <location filename="../../core/bug.cpp" line="44"/>
        <source>urgent</source>
        <translation>sehr hoch</translation>
    </message>
    <message>
        <location filename="../../core/bug.cpp" line="53"/>
        <source>not assigned</source>
        <translation>nicht zugewiesen</translation>
    </message>
    <message>
        <location filename="../../core/bug.cpp" line="54"/>
        <source>assigned</source>
        <translation>zugewiesen</translation>
    </message>
    <message>
        <location filename="../../core/bug.cpp" line="55"/>
        <source>fixing</source>
        <translation>unter Bearbeitung</translation>
    </message>
    <message>
        <location filename="../../core/bug.cpp" line="56"/>
        <source>fixed</source>
        <translation>ausgebessert</translation>
    </message>
    <message>
        <location filename="../../core/bug.cpp" line="57"/>
        <source>closed</source>
        <translation>geschlossen</translation>
    </message>
    <message>
        <location filename="../../core/user.cpp" line="27"/>
        <source>Guest</source>
        <translation>Gast</translation>
    </message>
    <message>
        <location filename="../../core/user.cpp" line="28"/>
        <source>Admin</source>
        <translation>Administrator</translation>
    </message>
    <message>
        <location filename="../../core/user.cpp" line="29"/>
        <source>Developer</source>
        <translation>Entwickler</translation>
    </message>
    <message>
        <location filename="../../core/user.cpp" line="30"/>
        <source>Chief Developer</source>
        <translation>Abteilungsleiter</translation>
    </message>
    <message>
        <location filename="../../core/user.cpp" line="31"/>
        <source>Tester</source>
        <translation>Tester</translation>
    </message>
    <message>
        <location filename="../../core/user.cpp" line="62"/>
        <source>nobody</source>
        <translation>niemand</translation>
    </message>
    <message>
        <location filename="../../util/helpbrowser.cpp" line="57"/>
        <source>BugTracker</source>
        <translation>BugTracker</translation>
    </message>
    <message>
        <location filename="../../util/helpbrowser.cpp" line="58"/>
        <source>Unable to launch Qt Assistant (%1)</source>
        <translation>Qt Assistant konnte nicht gestartet werden (%1)</translation>
    </message>
    <message>
        <location filename="../../util/database.cpp" line="72"/>
        <source>A database error occured:

</source>
        <translation>Ein Datenbankfehler trat auf:

</translation>
    </message>
</context>
<context>
    <name>Topframe</name>
    <message>
        <source>Refresh Buglist</source>
        <translation type="obsolete">Aktualisiere Bugliste</translation>
    </message>
    <message>
        <source>&amp;Refresh Buglist</source>
        <translation type="obsolete">&amp;Aktualisiere Bug-Liste</translation>
    </message>
    <message>
        <location filename="../../ui/topframe.cpp" line="39"/>
        <source>New Bug</source>
        <translation>Neuer Bug</translation>
    </message>
    <message>
        <location filename="../../ui/topframe.cpp" line="45"/>
        <source>New User</source>
        <translation>Neuer Nutzer</translation>
    </message>
    <message>
        <location filename="../../ui/topframe.cpp" line="50"/>
        <source>Configure</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../../ui/topframe.cpp" line="55"/>
        <source>Close</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location filename="../../ui/topframe.cpp" line="61"/>
        <source>Help</source>
        <translation>Hilfe</translation>
    </message>
    <message>
        <location filename="../../ui/topframe.cpp" line="63"/>
        <source>Show Documentation</source>
        <translation>Zeige Dokumentation</translation>
    </message>
    <message>
        <location filename="../../ui/topframe.cpp" line="68"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="../../ui/topframe.cpp" line="72"/>
        <source>About Qt</source>
        <translation>Über Qt</translation>
    </message>
    <message>
        <location filename="../../ui/topframe.cpp" line="200"/>
        <source>&amp;Bug Overview</source>
        <translation>&amp;Bug-Übersicht</translation>
    </message>
    <message>
        <location filename="../../ui/topframe.cpp" line="142"/>
        <source>Logged in as: </source>
        <translation>Angemeldet als:</translation>
    </message>
    <message>
        <location filename="../../ui/topframe.cpp" line="201"/>
        <location filename="../../ui/topframe.cpp" line="229"/>
        <source>Your personal overview of current happenings</source>
        <translation>Ihre persönliche Übersicht über aktuelle Ereignisse</translation>
    </message>
    <message>
        <location filename="../../ui/topframe.cpp" line="228"/>
        <source>&amp;Bug List</source>
        <translation>&amp;Bug-Liste</translation>
    </message>
    <message>
        <location filename="../../ui/topframe.cpp" line="253"/>
        <source>Bug #%1</source>
        <translation>Bug #%1</translation>
    </message>
    <message>
        <location filename="../../ui/topframe.cpp" line="276"/>
        <source>File #%1</source>
        <translation>Datei #%1</translation>
    </message>
    <message>
        <location filename="../../ui/topframe.cpp" line="283"/>
        <source>BugTracker</source>
        <translation>BugTracker</translation>
    </message>
    <message>
        <location filename="../../ui/topframe.cpp" line="284"/>
        <source>This is a project created by a team of students of the TechnicalUniversity of Chemnitz. The goal is a simple bug tracking softwarewhich could be used in software development teams. It is writtenin C++ by use of the Qt-framework.


Use it without any warrenty.</source>
        <translation>Dies ist ein Projekt, was von einer Gruppe von Studenten der Technischen Universität Chemnitz entwickelt wurde. Das Ziel ist eine schlichte Fehlerverfolgungssoftware, die in Softwareentwicklungsteams verwendet werden kann. Es ist in C++ entwickelt und basiert auf dem Qt-Framework.


Benutze es ohne Garantien.</translation>
    </message>
    <message>
        <location filename="../../ui/topframe.cpp" line="336"/>
        <source>&amp;Quit</source>
        <translation>&amp;Beenden</translation>
    </message>
    <message>
        <location filename="../../ui/topframe.cpp" line="337"/>
        <source>Warning: Close</source>
        <translation>Warnung: Beenden</translation>
    </message>
    <message>
        <location filename="../../ui/topframe.cpp" line="338"/>
        <source>Are you sure you want to quit the application?</source>
        <translation>Sind Sie sicher, dass Sie das Programm beenden möchten?</translation>
    </message>
    <message>
        <location filename="../../ui/topframe.cpp" line="37"/>
        <source>Application</source>
        <translation>Programm</translation>
    </message>
    <message>
        <source>Indicated messages shown here...</source>
        <translation type="obsolete">Anliegende Nachrichten werden hier angezeigt...</translation>
    </message>
    <message>
        <source>Messages</source>
        <translation type="obsolete">Nachrichten</translation>
    </message>
    <message>
        <source>No Bugs here... please refresh!</source>
        <translation type="obsolete">Keine Bugs hier... bitte aktualisieren!</translation>
    </message>
    <message>
        <source>Current Bugs</source>
        <translation type="obsolete">Aktuelle Bugs</translation>
    </message>
    <message>
        <source>Don&apos;t know...</source>
        <translation type="obsolete">Keine Ahnung...</translation>
    </message>
    <message>
        <source>no title</source>
        <translation type="obsolete">kein Titel</translation>
    </message>
    <message>
        <source>Want to refresh, eh?</source>
        <translation type="obsolete">Aktualisieren?</translation>
    </message>
    <message>
        <source>Sorry, but the refresh function is due to obvious issues not implemented.</source>
        <translation type="obsolete">Entschuldigen Sie, aber die Aktualisierungsfunktion ist aufgrund naheliegender Hinweise nicht implementiert.</translation>
    </message>
</context>
<context>
    <name>UserDialog</name>
    <message>
        <location filename="../../ui/dialog/userdialog.cpp" line="13"/>
        <source>Guest</source>
        <translation>Gast</translation>
    </message>
    <message>
        <location filename="../../ui/dialog/userdialog.cpp" line="14"/>
        <source>Administrator</source>
        <translation>Administrator</translation>
    </message>
    <message>
        <location filename="../../ui/dialog/userdialog.cpp" line="15"/>
        <source>Developer</source>
        <translation>Entwickler</translation>
    </message>
    <message>
        <location filename="../../ui/dialog/userdialog.cpp" line="16"/>
        <source>Chief-Developer</source>
        <translation>Abteilungsleiter</translation>
    </message>
    <message>
        <location filename="../../ui/dialog/userdialog.cpp" line="17"/>
        <source>Tester</source>
        <translation>Tester</translation>
    </message>
    <message>
        <location filename="../../ui/dialog/userdialog.cpp" line="26"/>
        <source>Login name</source>
        <translation>Login-Name</translation>
    </message>
    <message>
        <location filename="../../ui/dialog/userdialog.cpp" line="27"/>
        <source>Password</source>
        <translation>Passwort</translation>
    </message>
    <message>
        <location filename="../../ui/dialog/userdialog.cpp" line="28"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../../ui/dialog/userdialog.cpp" line="54"/>
        <source>Missing Information</source>
        <translation>Fehlende Informationen</translation>
    </message>
    <message>
        <location filename="../../ui/dialog/userdialog.cpp" line="55"/>
        <source>Your information could not be submitted due to missing values!</source>
        <translation>Ihre Informationen konnten aufgrund von fehlenden Informationen nicht gesendet werden!</translation>
    </message>
    <message>
        <location filename="../../ui/dialog/userdialog.cpp" line="65"/>
        <source>Error on submitting!</source>
        <translation>Fehler beim Übertragen!</translation>
    </message>
    <message>
        <location filename="../../ui/dialog/userdialog.cpp" line="66"/>
        <source>There was an error while submitting!</source>
        <translation>Es trat ein Fehler beim Übertragen auf!</translation>
    </message>
    <message>
        <location filename="../../ui/dialog/userdialog.cpp" line="72"/>
        <source>Help: Add a new user</source>
        <translation>Hilfe: Einen neuen Nutzer hinzufügen</translation>
    </message>
    <message>
        <location filename="../../ui/dialog/userdialog.cpp" line="73"/>
        <source>Only administrators are allowed to do this.

The login name and password will be required to log into the database.

The user type specifies which rights the user gets.</source>
        <translation>Nur Administratoren haben die Rechte hierzu.

Der Login-Name und das Passwort werden benötigt, um sich mit der Datenbank zu verbinden.

Der Nutzertyp legt fest, welche Rechte der Nutzer hat.</translation>
    </message>
</context>
<context>
    <name>UserPage</name>
    <message>
        <source>User information</source>
        <translation type="obsolete">Nutzerinformation</translation>
    </message>
    <message>
        <source>Login information into the database</source>
        <translation type="obsolete">Anmeldeinformation in die Datenbank</translation>
    </message>
    <message>
        <source>your name</source>
        <translation type="obsolete">Ihr Name</translation>
    </message>
    <message>
        <source>login name</source>
        <translation type="obsolete">Login-Name</translation>
    </message>
    <message>
        <source>password</source>
        <translation type="obsolete">Passwort</translation>
    </message>
    <message>
        <source>hidden</source>
        <translation type="obsolete">verborgen</translation>
    </message>
</context>
</TS>
