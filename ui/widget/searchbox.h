#ifndef SEARCHBOX_H
#define SEARCHBOX_H

#include <QWidget>
#include <QLineEdit>
#include <QPushButton>
#include <QCompleter>

class SearchBox : public QWidget
{
    Q_OBJECT
public:
    explicit SearchBox(QWidget *parent = 0);
    explicit SearchBox(QAbstractItemModel* model, int column, QWidget* parent = 0);

    void setCompleterData(QAbstractItemModel* model, int column = -1);
    void setModel(QAbstractItemModel* model);
    void setModelSorting(QCompleter::ModelSorting sorting);
    bool setCurrentRow(int row);
    void setCaseSensivity(Qt::CaseSensitivity caseSensitivity = Qt::CaseInsensitive);
    void setCompletionMode(QCompleter::CompletionMode mode = QCompleter::PopupCompletion);
    void setMaxVisibleItems(int maxItems = 5);
    void setPlaceholderText(const QString text = "Search ...");
    QString text() const;
signals:
    void textChanged();

private slots:
    void displayButton();
    void clearLine();

protected:
    void resizeEvent(QResizeEvent *);
    void keyPressEvent(QKeyEvent *);

private:
    QCompleter*  completer;
    QLineEdit*   lineEdit;
    QPushButton* button;
    QString      phText;
};

#endif // SEARCHBOX_H
