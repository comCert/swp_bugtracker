#ifndef DOCK_H
#define DOCK_H

#include <QDockWidget>
#include <QPushButton>
#include <QLabel>
#include <QList>
#include <QHBoxLayout>

class User;

class Dock : public QDockWidget
{
    Q_OBJECT
public:
    Dock(QWidget* parent = 0);
    Qt::DockWidgetArea getDockPosition();
signals:
    void viewClicked();
    void listClicked();
    void nBugClicked();
private slots:
    void onLoggedIn(User *);
    void adjustDirection(Qt::DockWidgetArea area);
    void propagateView();
    void propagateList();
    void propagateNewBug();
    void doLogin();
private:
    QBoxLayout* layout;
    QPushButton* bOverview;
    QPushButton* bList;
    QPushButton* bNewBug;
    QPushButton* login;

    Qt::DockWidgetArea dockPos;
};

#endif // DOCK_H
