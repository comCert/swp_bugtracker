#include "buginfo.h"
#include "session.h"
#include "user.h"
#include "attachment.h"
#include "comment.h"

 #include <QAbstractItemModel>

BugInfo::BugInfo(Bug * b, QWidget * parent)
    : QScrollArea(parent)
    , bug(b)
{
    setWidgetResizable(true);
    QLabel * label;
    QVBoxLayout *mainLayout = new QVBoxLayout;
    QGroupBox *formInfoBox, *formDescriptionBox;
    QGroupBox *formAttachmentBox, *formCommentBox;
    QPushButton * button;

    viewport = new QWidget;
    formInfoBox = new QGroupBox(tr("Info"));
    QFormLayout *layout = new QFormLayout;
    layout->setFormAlignment(Qt::AlignRight);
    layout->setContentsMargins(20,10,50,0);

    // title
    label = new QLabel(b->getTitle());
    label->setWordWrap(true);
    layout->addRow(tr("Title:"), label);

    // date
    label = new QLabel(b->getDate().toString());
    layout->addRow(tr("Date:"), label);

    // version
    label = new QLabel(b->getVersion());
    layout->addRow(tr("Version:"), label);

    // module
    label = new QLabel(b->getModule());
    layout->addRow(tr("Module:"), label);

    // status
    statuslayout = new QHBoxLayout;
    statuslabel = new QLabel(toString(b->getStatus()));
    bGrab    = new QPushButton(tr("Grab"));
    bAccept  = new QPushButton(tr("Accept"));
    bDone    = new QPushButton(tr("Done"));
    bConfirm = new QPushButton(tr("Confirm"));
    bReject  = new QPushButton(tr("Reject"));

    QButtonGroup * bg = new QButtonGroup(this);
    bg->addButton(bGrab, 0);
    bg->addButton(bAccept, 1);
    bg->addButton(bDone, 2);
    bg->addButton(bConfirm, 3);
    bg->addButton(bReject, 4);
    connect(bg, SIGNAL(buttonClicked(int)),
            this, SLOT(changeStatus(int)));

    statuslayout->addWidget(statuslabel, 0, Qt::AlignLeft);
    statuslayout->addWidget(bGrab);
    statuslayout->addWidget(bAccept);
    statuslayout->addWidget(bDone);
    statuslayout->addWidget(bConfirm);
    statuslayout->addWidget(bReject);
    updateStatus();
    layout->addRow(tr("Status:"), statuslayout);

    // priority
    if(hasPerm("bug.change-priority")) {
        QHBoxLayout * hl = new QHBoxLayout;
        QComboBox * cb = new QComboBox;
        // TODO: replace with proper (and persistent) data model
        cb->addItem(toString(BP_LOW));
        cb->addItem(toString(BP_DEFAULT));
        cb->addItem(toString(BP_HIGH));
        cb->addItem(toString(BP_URGENT));
        cb->setCurrentIndex(b->getPriority());
        hl->addWidget(cb);
        hl->addStretch(1);
        layout->addRow(tr("Priority:"), hl);

        connect(cb, SIGNAL(currentIndexChanged(int)),
                this, SLOT(changePriority(int)));
    }
    else {
        label = new QLabel(toString(b->getPriority()));
        layout->addRow(tr("Priority:"), label);
    }

    // tester
    label = new QLabel(b->getTester()->getUsername());
    layout->addRow(tr("Tester:"), label);

    // developer
    if(hasPerm("bug.change-developer"))
    {
        QSqlQueryModel * devmodel = new QSqlQueryModel(this);
        devmodel->setQuery(
            "SELECT id, username FROM user WHERE type=2 OR type=3");
        devcombo = new QComboBox;
        QHBoxLayout * hl = new QHBoxLayout;
        QPushButton * bu = new QPushButton(tr("Assign"));
        devcombo->setModel(devmodel);
        devcombo->setModelColumn(1);
        int index = devcombo->findText(b->getDeveloper()->getUsername());
        if(index != -1)
        {
            devcombo->setCurrentIndex(index);
            devlabel = NULL;
        }
        else
        {
            devlabel = new QLabel(bug->getDeveloper()->getUsername());
            hl->addWidget(devlabel);
        }

        connect(bu, SIGNAL(clicked()), this, SLOT(assignBug()));
        hl->addWidget(devcombo);
        hl->addWidget(bu);
        hl->addStretch(1);
        layout->addRow(tr("Developer:"), hl);
    }
    else
    {
        devlabel = new QLabel(b->getDeveloper()->getUsername());
        layout->addRow(tr("Developer:"), devlabel);
    }
    formInfoBox->setLayout(layout);


    // description
    layout = new QFormLayout;
    layout->setContentsMargins(20,10,50,0);
    formDescriptionBox = new QGroupBox(tr("Description"));
    label = new QLabel(b->getDescription());
    label->setWordWrap(true);
    layout->addRow(label);
    formDescriptionBox->setLayout(layout);


    // attachments
    layout            = new QFormLayout;
    formAttachmentBox = new QGroupBox(tr("Attachments"));
    attmodel          = new AttachmentTableModel(this);
    attproxy          = new QSortFilterProxyModel(this);
    attview           = new QTableView;

    attmodel->setData( b->getAttachments() );
    attproxy->setSourceModel(attmodel);
    attview->setModel(attproxy);
    attview->setMaximumHeight(100);
    attview->horizontalHeader()->setResizeMode(QHeaderView::ResizeToContents);
    attview->horizontalHeader()->setResizeMode(1, QHeaderView::Stretch);
    attview->resizeColumnsToContents();
    connect(attview, SIGNAL(doubleClicked(QModelIndex)),
            this   , SLOT(selectAttachment(QModelIndex)) );
    layout->setContentsMargins(20,10,50,0);
    layout->addRow(attview);
    if(hasPerm("attachment.create"))
    {
        QHBoxLayout * hboxlayout = new QHBoxLayout;
        attachmentEdit = new QLineEdit;
        //connect(attachmentEdit, SIGNAL(clicked()),
        //        this, SLOT(openFileDialog()));
        hboxlayout->addWidget(attachmentEdit);
        button = new QPushButton(tr("Browse"));
        connect(button, SIGNAL(clicked()),
                this  , SLOT(openFileDialog()));
        hboxlayout->addWidget(button);
        button = new QPushButton(tr("Upload"));
        connect(button, SIGNAL(clicked()),
                this  , SLOT(uploadFile()));
        hboxlayout->addWidget(button);
        layout->addRow(hboxlayout);
    }
    formAttachmentBox->setLayout(layout);


    // comments
    layout         = new QFormLayout;
    commentLayout  = new QVBoxLayout;
    formCommentBox = new QGroupBox(tr("Comments"));
    layout->setContentsMargins(20,10,50,0);
    layout->addRow(commentLayout);

    if(hasPerm("comment.create"))
    {
        commentEdit = new QPlainTextEdit;
        commentEdit->setMaximumHeight(60);
        button = new QPushButton(tr("Submit"));
        connect(button, SIGNAL(clicked()),
                this  , SLOT(submitComment()));
        layout->addRow(commentEdit);
        layout->addWidget(button);
        layout->setAlignment(button, Qt::AlignLeft);
    }
    formCommentBox->setLayout(layout);

    comments = b->getComments();
    if(comments.empty()) {
        label = new QLabel(tr("no comments"));
        commentLayout->addWidget(label);
    }
    else {
        QList<Comment *>::iterator i, e;
        for(i = comments.begin(), e = comments.end(); i != e; ++i)
            addCommentWidget(*i);
    }

    mainLayout->addWidget(formInfoBox);
    mainLayout->addWidget(formDescriptionBox);
    mainLayout->addWidget(formAttachmentBox);
    mainLayout->addWidget(formCommentBox);
    mainLayout->addStretch(1);

    viewport->setLayout(mainLayout);
    this->setWidget(viewport);
}

BugInfo::~BugInfo()
{
    for(QList<Comment *>::iterator i = comments.begin(),
        e = comments.end(); i != e; ++i)
    {
        delete *i;
    }
}

void BugInfo::addCommentWidget(const Comment * comment)
{
    QLabel * label;

    label = new QLabel(tr("<i>%1</i> wrote on %2:")
        .arg(comment->getAuthor()->getUsername())
        .arg(comment->getDate().toString()));
    label->setWordWrap(true);
    label->setTextInteractionFlags(Qt::TextSelectableByMouse);
    commentLayout->addWidget(label);

    label = new QLabel(comment->getText());
    label->setStyleSheet("padding:5px; margin-bottom:10px;");
    label->setWordWrap(true);
    label->setTextInteractionFlags(Qt::TextSelectableByMouse);
    commentLayout->addWidget(label);
    viewport->adjustSize();
}

void BugInfo::openFileDialog()
{
    QString path = QFileDialog::getOpenFileName(this, tr("Select File"));
    attachmentEdit->setText(path);
}

void BugInfo::uploadFile()
{
    QString path = attachmentEdit->text();
    if(path.isEmpty())
        return;

    Attachment * a = bug->addAttachment(path);
    if(a == NULL)
        return;
    attmodel->addData(a);
    attachmentEdit->setText("");
}

void BugInfo::submitComment()
{
    QString text = commentEdit->toPlainText();
    if(text.isEmpty())
        return;

    Comment * c = bug->addComment(text);
    if(c == NULL)
        return;

    // remove the "no comments" notice
    if(comments.empty())
        commentLayout->takeAt(0)->widget()->hide();

    addCommentWidget(c);
    comments.append(c);
}

void BugInfo::selectAttachment(const QModelIndex & index)
{
    QModelIndex i = attproxy->mapToSource(index);
    Attachment * a = attmodel->getData()[i.row()];
    emit attachmentSelected(a);
}

void BugInfo::assignBug()
{
    QAbstractItemModel * m = devcombo->model();
    QModelIndex i = m->index( devcombo->currentIndex(), 0 );
    int dev_id = m->data(i).toInt();
    User * u = User::getByID(dev_id);
    if(u != NULL)
    {
        if(bug->getDeveloper()->getID() == -1)
            devlabel->hide();
        u->assignBug(bug);
        bug->changeStatus(BS_ASSIGNED);
        updateStatus();
    }
    else
        printf("error\n");
}

void BugInfo::updateStatus()
{
    User & u = Session::getUser();
    BugStatus bs = bug->getStatus();

    statuslabel->setText( toString(bs) );

    bGrab->hide();
    bAccept->hide();
    bDone->hide();
    bConfirm->hide();
    bReject->hide();

    switch(u.getType())
    {
    case UT_DEVELOPER:
    case UT_CHIEF_DEVELOPER:
        if(bs == BS_NOT_ASSIGNED)
            bGrab->show();
        if(bs == BS_ASSIGNED && bug->getDeveloper()->getID() == u.getID())
            bAccept->show();
        if(bs == BS_FIXING && bug->getDeveloper()->getID() == u.getID())
            bDone->show();
        break;

    case UT_TESTER:
        if(bs == BS_FIXED && bug->getTester()->getID() == u.getID())
        {
            bConfirm->show();
            bReject->show();
        }
        break;

    default:
        break;
    }
}

void BugInfo::changeStatus(int button_id)
{
    printf("status: %d\n", button_id);
    switch(button_id)
    {
    case 0: bug->changeStatus(BS_FIXING);
            Session::getUser().assignBug(bug);
            devlabel->setText(Session::getUser().getUsername());
            break;
    case 1: bug->changeStatus(BS_FIXING); break;
    case 2: bug->changeStatus(BS_FIXED);  break;
    case 3: bug->changeStatus(BS_CLOSED); break;
    case 4: bug->changeStatus(BS_FIXING); break;
    }
    updateStatus();
}

void BugInfo::changePriority(int i)
{
    bug->changePriority(toBugPriority(i));
}

const Bug * BugInfo::getBug() const
{
    return bug;
}
