#include "buglist.h"
#include "session.h"
#include <QtGui>

BugList::BugList(QWidget * parent)
    : QWidget(parent)
{
    QVBoxLayout * vLayout = new QVBoxLayout();
    QHBoxLayout * hLayout = new QHBoxLayout();

    /// create widgets
    cFilterCombo = new QComboBox(this);
    sFilterBox   = new SearchBox(this);
    proxyModel   = new QSortFilterProxyModel(this);
    model        = new BugTableModel(this);
    view         = new QTableView(this);
    refreshButton= new QPushButton(tr("&Refresh"), this);

    /// initialize tableview
    proxyModel->setSourceModel(model);
    view->setModel(proxyModel);
    view->hideColumn(2);
    view->horizontalHeader()->setVisible(true);
    view->horizontalHeader()->setResizeMode(0, QHeaderView::ResizeToContents);
    view->horizontalHeader()->setResizeMode(BMC_LAST-1, QHeaderView::Stretch);
    view->verticalHeader()->hide();
    view->setSortingEnabled(true);
    connect(view, SIGNAL(doubleClicked(QModelIndex)),
            this, SLOT(selectBug(QModelIndex)) );

    /// initialize timer
    timer = new QTimer(this);
    timer->start(300000); // 5 min Intervall
    connect(timer, SIGNAL(timeout()), this, SLOT(refresh()));

    /// initialize refreshButton
    connect(refreshButton, SIGNAL(clicked()),
            this,          SLOT(refresh()));

    for (int i = 0; i < proxyModel->columnCount(); ++i) {
        cFilterCombo->addItem(proxyModel->headerData(i, Qt::Horizontal).toString());
    }
    sFilterBox->setPlaceholderText(tr("Filter ..."));
    sFilterBox->setModel(proxyModel);
    sFilterBox->setModelSorting(QCompleter::CaseSensitivelySortedModel);
    sFilterBox->setCompletionMode(QCompleter::PopupCompletion);

    connect(cFilterCombo, SIGNAL(currentIndexChanged(int)),
            this, SLOT(setFilterColumn(int)));
    connect(sFilterBox, SIGNAL(textChanged()),
            this, SLOT(setFilterRegex()));

    /// set layout
    hLayout->addWidget(cFilterCombo);
    hLayout->addWidget(sFilterBox);
    hLayout->addStretch(1);
    vLayout->addLayout(hLayout);
    vLayout->addWidget(view);
    vLayout->addWidget(refreshButton, 0, Qt::AlignRight);
    setLayout(vLayout);

    /// fill list with data
    refresh();
}

void BugList::refresh()
{
    QSqlQuery q;
    QList<Bug *> l;
    if(Session::getDatabase().performQuery(
        "SELECT   id, title, description, module, version, date,"
        "         state, priority, tester_id, developer_id "
        "FROM     bug ", &q))
    {
        while(q.next())
            l.append(new Bug(q));
        model->setData(l);
    }
    view->resizeColumnsToContents();
}

void BugList::selectBug(const QModelIndex & index)
{
    // get bug specified by 'index' ...
    QModelIndex i = proxyModel->mapToSource(index);
    Bug * b = model->getData().at(i.row());
    // and (re-)emit signal
    emit bugSelected(b);
}

void BugList::setFilterRegex()
{
    QString text = sFilterBox->text();
    proxyModel->setFilterRegExp(text);
}

void BugList::setFilterColumn(int i)
{
    proxyModel->setFilterKeyColumn(i);
    sFilterBox->setCompleterData(proxyModel, i);
}
