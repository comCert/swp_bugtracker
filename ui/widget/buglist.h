#ifndef BUGLIST_H
#define BUGLIST_H

#include "bug.h"
#include "bugtablemodel.h"
#include "searchbox.h"

#include <QtGui>

/*
 * class BugList
 *
 * This widget shall provide an overview over all bugs currently tracked by the
 * system. Information is presented in a tabular format.
 * Users may utilize the provided search features to ... ???
 *
 */
class BugList : public QWidget
{
    Q_OBJECT

public:
    BugList(QWidget * parent = 0);
    void createExampleData();

signals:
    void bugSelected(Bug *);

public slots:
    //
    void refresh();

private slots:
    // this slot only emits the 'bugSelected' signals for the bug specified by
    // the QModelIndex
    void selectBug(const QModelIndex & index);
    void setFilterRegex();
    void setFilterColumn(int);

private:
    // names should be replaced by better one, if function is known
    QComboBox     * cFilterCombo;
    SearchBox     * sFilterBox;
    QPushButton   * refreshButton;
    QSortFilterProxyModel * proxyModel;
    BugTableModel * model;
    QTableView    * view;
    QTimer        * timer;
};

#endif // BUGLIST_H
