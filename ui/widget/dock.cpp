#include "dock.h"
#include "dialog/bugdialog.h"
#include "dialog/logindialog.h"
#include "session.h"
#include <QtGui>

Dock::Dock(QWidget* parent)
    : QDockWidget(parent)
{
    setAllowedAreas(Qt::AllDockWidgetAreas);
    setFeatures(QDockWidget::AllDockWidgetFeatures);

    bOverview = new QPushButton(tr("Overview"), this);
    bOverview->setToolTip(tr("Your personal overview of current happenings"));
    connect(bOverview, SIGNAL(clicked()), this, SLOT(propagateView()));
    bList = new QPushButton(tr("List"), this);
    bList->setToolTip(tr("List of all bugs"));
    connect(bList, SIGNAL(clicked()), this, SLOT(propagateList()));
    bNewBug = new QPushButton(tr("Commit Bug"), this);
    bNewBug->setIcon(QIcon(":/png/addDb"));
    bNewBug->setToolTip(tr("Opens a form to create a new bug entry"));
    connect(bNewBug, SIGNAL(clicked()), this, SLOT(propagateNewBug()));

    login = new QPushButton(tr("Login"));
    connect(login, SIGNAL(clicked()), this, SLOT(doLogin()));

    connect(this, SIGNAL(dockLocationChanged(Qt::DockWidgetArea)), this, SLOT(adjustDirection(Qt::DockWidgetArea)));
    connect(&Session::instance, SIGNAL(loggedIn(User*)),
            this, SLOT(onLoggedIn(User*)));
    QWidget* widget = new QWidget(this);
    layout = new QBoxLayout(QBoxLayout::LeftToRight);
    layout->addWidget(bOverview);
    layout->addWidget(bList);
    layout->addWidget(bNewBug);
    layout->addStretch(1);
    layout->addWidget(login);
    widget->setLayout(layout);
    setWidget(widget);
}

Qt::DockWidgetArea Dock::getDockPosition()
{
    return dockPos;
}

void Dock::onLoggedIn(User * u)
{
    bNewBug->setEnabled( u->hasPermission("bug.create") );

    if(u->getUsername() == "guest")
        login->setText(tr("Login"));
    else
        login->setText(tr("Logout"));
}

void Dock::adjustDirection(Qt::DockWidgetArea area)
{
    switch (area) {
    default :
    case Qt::LeftDockWidgetArea : // falls through
    case Qt::RightDockWidgetArea :
        layout->setDirection(QBoxLayout::TopToBottom);
        break;
    case Qt::TopDockWidgetArea : // falls through
    case Qt::BottomDockWidgetArea :
        layout->setDirection(QBoxLayout::LeftToRight);
        break;
    case Qt::NoDockWidgetArea :
        layout->setDirection(QBoxLayout::TopToBottom);
        break;
    }

    setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    dockPos = area;
}

void Dock::propagateView()
{
    emit viewClicked();
}

void Dock::propagateList()
{
    emit listClicked();
}

void Dock::propagateNewBug()
{
    emit nBugClicked();
}

void Dock::doLogin()
{
    if(Session::getUser().getUsername() == "guest")
    {
        LoginDialog ld;
        if(ld.exec())
        {
            if(Session::login(ld.username(), ld.password()) == false)
            {
                QMessageBox::critical(0, tr("Login failed"),
                tr("Login failed"), QMessageBox::Ok, QMessageBox::NoButton);
                Session::login("guest", "guest");
            }
        }
    }
    else
    {
        Session::logout();
    }
}
