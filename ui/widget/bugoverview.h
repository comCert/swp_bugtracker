#ifndef BUGOVERVIEW_H
#define BUGOVERVIEW_H

#include <QtGui>
#include <QtSql>

#include "bugtablemodel.h"
#include "comment.h"

class User;

class BugOverview : public QWidget
{
    Q_OBJECT

public:

    BugOverview(QWidget * parent = 0);

public slots:

    void refresh();
    void selectBug(const QModelIndex& index);
    void selectComment(const QModelIndex& index);

signals:
    void bugSelected(Bug *b);

private:

    BugTableModel  * bugModel;
    QTableView     * bugView;
    QSqlQueryModel * commentModel;
    QTableView     * commentView;

};

#endif /* BUGOVERVIEW_H */
