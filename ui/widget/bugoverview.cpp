#include "bugoverview.h"
#include "session.h"
#include "user.h"
#include "bug.h"

BugOverview::BugOverview(QWidget * parent)
    : QWidget(parent)
{

    QVBoxLayout * layout = new QVBoxLayout;
    QLabel * l1 = new QLabel(tr("<strong>Recent Bugs</strong>"));
    QLabel * l2 = new QLabel(tr("<strong>Recent Comments</strong>"));
    QPushButton * b = new QPushButton(tr("Refresh"));

    bugModel     = new BugTableModel(this);
    bugView      = new QTableView(this);
    commentModel = new QSqlQueryModel(this);
    commentView  = new QTableView(this);

    bugView->setModel(bugModel);
    bugView->hideColumn(2);
    bugView->horizontalHeader()->setVisible(true);
    bugView->verticalHeader()->hide();
    bugView->horizontalHeader()->setResizeMode(0, QHeaderView::ResizeToContents);
    bugView->horizontalHeader()->setResizeMode(BMC_LAST-1, QHeaderView::Stretch);
    connect(bugView, SIGNAL(doubleClicked(QModelIndex)),
            this, SLOT(selectBug(QModelIndex)));

    commentView->setModel(commentModel);
    commentView->horizontalHeader()->setVisible(true);
    commentView->verticalHeader()->hide();
    commentView->horizontalHeader()->setResizeMode(QHeaderView::ResizeToContents);
    commentView->horizontalHeader()->setResizeMode(1, QHeaderView::Stretch);
    connect(commentView, SIGNAL(doubleClicked(QModelIndex)),
            this, SLOT(selectComment(QModelIndex)));

    connect(b, SIGNAL(clicked()), this, SLOT(refresh()));

    layout->addWidget(l1);
    layout->addWidget(bugView);
    layout->addWidget(l2);
    layout->addWidget(commentView);
    layout->addWidget(b, 0, Qt::AlignRight);
    setLayout(layout);
}

void BugOverview::refresh()
{
    User & u = Session::getUser();

    QList<Bug *> bugs = u.getBugs();
    bugModel->setData(bugs);

    // TODO
    QSqlQuery q;
    q.prepare(
        "SELECT user1.username AS Author, comment.text AS Message,"
        "       comment.date AS Date, bug.title AS Bug "
        "FROM   comment, user, bug "
        "WHERE  comment.bug_id=bug.id AND comment.author_id=user.id AND "
        "       (bug.tester_id=? OR bug.developer_id=?) "
        "ORDER BY date DESC "
        "LIMIT  5");
    q.addBindValue(u.getID());
    q.addBindValue(u.getID());
    q.exec();
    commentModel->setQuery(q);

}

void BugOverview::selectBug(const QModelIndex &index)
{
    Bug* b = bugModel->getData().at(index.row());
    emit bugSelected(b);
}

void BugOverview::selectComment(const QModelIndex &index)
{
    // get bug_id from comment model
    //QSqlRecord rec = commentModel->record(index.row());
    // - missing bug_id in QueryModel
    //Bug* b;
    //emit(bugSelected(b));
}
