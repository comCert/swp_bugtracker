#ifndef BUGINFO_H
#define BUGINFO_H

#include "bug.h"
#include "attachmenttablemodel.h"
#include <QtGui>

class Attachment;
class Comment;

class BugInfo : public QScrollArea
{
    Q_OBJECT

public:
    BugInfo(Bug * b, QWidget * parent = 0);
    virtual ~BugInfo();

    const Bug * getBug() const;

signals:
    void attachmentSelected(Attachment *);

private slots:
    void openFileDialog();
    void uploadFile();
    void submitComment();
    void selectAttachment(const QModelIndex & index);
    void assignBug();
    void changeStatus(int);
    void changePriority(int);

private:
    void updateStatus();
    void addAttachmentWidget(const Attachment *);
    void addCommentWidget(const Comment *);

    Bug               * bug;
    QPushButton       * bGrab;
    QPushButton       * bAccept;
    QPushButton       * bDone;
    QPushButton       * bConfirm;
    QPushButton       * bReject;
    QLabel            * devlabel;
    QComboBox         * devcombo;
    QLabel            * statuslabel;
    QHBoxLayout       * statuslayout;
    QVBoxLayout       * attachmentLayout;
    QVBoxLayout       * commentLayout;
    QLineEdit         * attachmentEdit;
    QPlainTextEdit    * commentEdit;
    QTableView        * attview;
    QSortFilterProxyModel * attproxy;
    AttachmentTableModel * attmodel;
    QList<Comment *>    comments;
    QWidget * viewport;

};

#endif // BUGINFO_H
