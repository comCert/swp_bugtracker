#ifndef CLICKABLELABEL_H
#define CLICKABLELABEL_H

#include <QLabel>
#include <QCursor>
#include <QMouseEvent>
#include <QPalette>
#include <QEvent>

class ClickableLabel : public QLabel
{
    Q_OBJECT
public:
    explicit ClickableLabel(const QString& text = "", QWidget *parent = 0);
    ~ClickableLabel();
    
signals:
    void clicked();

protected:
    void mousePressEvent(QMouseEvent *ev);
    void mouseMoveEvent(QMouseEvent *ev);
    void enterEvent(QEvent *ev);
    void leaveEvent(QEvent *ev);

private:
};

#endif // CLICKABLELABEL_H
