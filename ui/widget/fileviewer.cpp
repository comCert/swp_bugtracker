#include "fileviewer.h"
#include "user.h"
#include <QtGui>
#include <QFileDialog>

FileViewer::FileViewer(const Attachment *file, QWidget *parent) :
    QWidget(parent),
    file(file)
{
    setMinimumSize(400, 350);

    QString str = file->getName() + " ("
            + file->getDate().toString(Qt::SystemLocaleShortDate) + ")";
    lLeftHint = new QLabel(str ,this);
    str = tr("by ") + file->getUploader()->getUsername();
    lRightHint = new QLabel(str, this);

    QHBoxLayout* topLayout = new QHBoxLayout;
    topLayout->addWidget(lLeftHint);
    topLayout->addStretch(1);
    topLayout->addWidget(lRightHint);

    bSaveLocal = new QPushButton(tr("Download"), this);
    bSaveLocal->setFlat(true);
    connect(bSaveLocal, SIGNAL(clicked()), this, SLOT(saveAs()));

    /*
     * TODO: checkout some configuration possibilities if needed or useful
     *      ~ maybe added to options dialog
     */
    // ~QGraphicsScene scene(this);
    // ~QGraphicsView view(&scene);
    QLabel * l = new QLabel;
    if (file->hasText()) {
        l->setText(file->text());
        // ~scene.addText(file->text());
    } else if (file->hasImage()) {
        QPixmap * pm = new QPixmap;
        pm->loadFromData( file->imageData().toByteArray() );
        l->setPixmap(*pm);
        // ~scene.addPixmap(qvariant_cast<QPixmap>(file->imageData()));
    }

    QVBoxLayout* layout = new QVBoxLayout;
    QHBoxLayout* bottomLayout = new QHBoxLayout;
    bottomLayout->addStretch(1);
    bottomLayout->addWidget(bSaveLocal);
    layout->addLayout(topLayout);
    layout->addWidget(l, 1, Qt::AlignCenter);
    layout->addLayout(bottomLayout);
    setLayout(layout);
}

void FileViewer::saveAs()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save as"));

    if (fileName.isEmpty()) {
        QMessageBox::warning(this, tr("Error while saving"),
                             tr("There was an error! File was not saved!"),
                             QMessageBox::Ok | QMessageBox::NoButton);
        return;
    }

    QFile f(fileName);
    if (!f.open(QFile::WriteOnly)) {
        QMessageBox::warning(this, tr("Error saving"),
                             tr("Cannot write file!"));
        return;
    }

    f.write(file->data(file->getMimeType()));
    return;
}
