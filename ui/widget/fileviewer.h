#ifndef FILEVIEWER_H
#define FILEVIEWER_H

#include <QWidget>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QPushButton>
#include <QTextBrowser>
#include <QLabel>
#include <QDateTime>

#include "attachment.h"

class FileViewer : public QWidget
{
    Q_OBJECT
public:
    explicit FileViewer(const Attachment* file, QWidget *parent = 0);

    const Attachment * getFile() const { return file; }

signals:

public slots:
    void saveAs();

private:
    const Attachment* file;
    QLabel*        lLeftHint;
    QLabel*        lRightHint;
    QWidget*       center;
    QPushButton*   bSaveLocal;
};

#endif // FILEVIEWER_H
