#include <QtGui>

#include "searchbox.h"

SearchBox::SearchBox(QWidget *parent) :
    QWidget(parent)
{
    lineEdit = new QLineEdit(this);
    lineEdit->setFixedHeight(30);
    lineEdit->setStyleSheet("QLineEdit {"
                            "}");

    completer = new QCompleter(this);
    completer->setCaseSensitivity(Qt::CaseInsensitive);
    completer->setModelSorting(QCompleter::CaseInsensitivelySortedModel);
    lineEdit->setCompleter(completer);

    button = new QPushButton(this);
    button->setIcon(QIcon(QPixmap(":/png/sbClear")));
    button->setFlat(true);
    button->setFocusPolicy(Qt::NoFocus);
    button->setFixedSize(22, 22);
    button->setVisible(false);
    button->setStyleSheet("QPushButton {"
                          "border-radius: 0px;"
                          "}");
    connect(button, SIGNAL(clicked()), this, SLOT(clearLine()));
    connect(lineEdit, SIGNAL(textChanged(QString)), this, SLOT(displayButton()));

    QHBoxLayout* layout = new QHBoxLayout;
    layout->addWidget(lineEdit);
    setLayout(layout);
}

SearchBox::SearchBox(QAbstractItemModel *model, int column, QWidget *parent) :
    QWidget(parent)
{
    lineEdit = new QLineEdit(this);
    lineEdit->setFixedHeight(30);

    completer = new QCompleter(this);
    completer->setModel(model);
    completer->setCompletionColumn(column);
    completer->setCaseSensitivity(Qt::CaseInsensitive);
    completer->setModelSorting(QCompleter::CaseInsensitivelySortedModel);
    lineEdit->setCompleter(completer);

    button = new QPushButton("X", this);
    button->setFlat(true);
    button->setFocusPolicy(Qt::NoFocus);
    button->setFixedSize(22, 22);
    button->setVisible(false);
    connect(button, SIGNAL(clicked()), this, SLOT(clearLine()));
    connect(lineEdit, SIGNAL(textChanged(QString)), this, SLOT(displayButton()));

    QHBoxLayout* layout = new QHBoxLayout;
    layout->addWidget(lineEdit);
    setLayout(layout);
}

QString SearchBox::text() const
{
    return lineEdit->text();
}

void SearchBox::setModel(QAbstractItemModel *model)
{
    completer->setModel(model);
}

void SearchBox::setModelSorting(QCompleter::ModelSorting sorting)
{
    completer->setModelSorting(sorting);
}

void SearchBox::setCompleterData(QAbstractItemModel *model, int column)
{
    completer->setModel(model);

    if (column != -1)
        completer->setCompletionColumn(column);
}

void SearchBox::setCompletionMode(QCompleter::CompletionMode mode)
{
    completer->setCompletionMode(mode);
}

void SearchBox::setCaseSensivity(Qt::CaseSensitivity caseSensitivity)
{
    completer->setCaseSensitivity(caseSensitivity);
}

void SearchBox::setMaxVisibleItems(int maxItems)
{
    completer->setMaxVisibleItems(maxItems);
}

void SearchBox::setPlaceholderText(const QString text)
{
    lineEdit->setPlaceholderText(text);
}

void SearchBox::resizeEvent(QResizeEvent *)
{
    button->move(lineEdit->pos().x()+lineEdit->width()-26,
             lineEdit->pos().y()+lineEdit->height()/2-11);
}

void SearchBox::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == QKeySequence::Find) {
        lineEdit->setFocus();
    } else if (event->key() == QKeySequence(Qt::CTRL + Qt::Key_C) &&
               lineEdit->hasFocus()) {
        clearLine();
    }
}

void SearchBox::displayButton()
{
    if (!lineEdit->text().isEmpty()) {
        button->setVisible(true);
    } else {
        button->setVisible(false);
    }
    emit(textChanged());
}

void SearchBox::clearLine()
{
    lineEdit->clear();
    button->setVisible(false);
}
