#include "clickablelabel.h"

ClickableLabel::ClickableLabel(const QString& text, QWidget *parent) :
    QLabel(parent)
{
    this->setText(text);
    this->setMouseTracking(true);
    this->setAttribute(Qt::WA_Hover);
}

ClickableLabel::~ClickableLabel()
{

}

void ClickableLabel::mousePressEvent(QMouseEvent *ev)
{
    emit clicked();
}

void ClickableLabel::mouseMoveEvent(QMouseEvent *ev)
{
    QCursor cur(Qt::PointingHandCursor);
    this->setCursor(cur);
}

void ClickableLabel::enterEvent(QEvent *ev)
{
    QPalette p;
    p.setColor(QPalette::WindowText, Qt::blue);
    setPalette(p);
}

void ClickableLabel::leaveEvent(QEvent *ev)
{
    QPalette p;
    p.setColor(QPalette::WindowText, Qt::black);
    setPalette(p);
}
