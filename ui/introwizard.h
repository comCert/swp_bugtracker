#ifndef INTROWIZARD_H
#define INTROWIZARD_H

#include <QWizard>
#include <QLabel>
#include <QLineEdit>
#include <QTextBrowser>
#include <QSettings>
#include <QPushButton>

/*!
 * \brief The IntroWizard class
 * This class provides the introduction assistant of the application.
 * It handles some userinput according to login information and also
 * first connection handling into the database.
 * It inherits the Q_OBJECT macro.
 * @see QWizard
 */
class IntroWizard : public QWizard
{
    Q_OBJECT

public:
    /*!
     * \brief IntroWizard standard constructor
     * A new IntroWizard must be created using this constructor.
     * \param parent The parameter pointer parent is used handling window modality.
     */
    IntroWizard(QWidget* parent = 0);
private slots:
    /*!
     * \brief fillGuestInfos() is called when clicking on the guest login button
     * It fills common data in the QLineEdits login name and both passwords.
     * Also updates the text of the guest login button, to disable it.
     */
    void fillGuestInfos();
    /*!
     * \brief checkLoginData() trys to connect to database.
     * Gets the information from user input and trys to connect to database, also given by user.
     * A label displays it's conclusion, soccuess or error.
     * @see Session
     */
    void checkLoginData();
    /*!
     * \brief generate(id) fills the information provided into the last pages overview.
     * \param id is needed, if last wizard page is actual one.
     * Called when wizard pages change.
     */
    void generate(int id);
    /*!
     * \brief updateConclusions()
     * Gets the information by user and generates a label and a string to fill in the last page.
     * Updates the local variables conLabel and conclusion.
     */
    void updateConclusions();
    /*!
     * \brief updatePassLabel() rates the current passwords.
     * Generates the label, which shows if the passwords match, missmatch or other things.
     */
    void updatePassLabel();
    /*!
     * \brief help() is requested when clicking on the help button.
     * Called by QIntroWizard::helpRequested(). Every page has it's own help box.
     */
    void help();
protected:
    //! \brief accept() is called on finishing the wizard.
    void accept();
    //! \brief reject() is called on closing the wizard.
    void reject();
    //! \brief It is a reimplemention of QWidget::closeEvent().
    void closeEvent(QCloseEvent *e);
    //! \brief Writes the provided user information to settings file.
    void storeSettings();
private:
    //! \brief Creates the interface of the wizard.
    void     setupPages();
    QString& generateConclusion();
    QLabel&  getConclusionLabel();

    QWizardPage* introPage;
    QLabel*      i_l;

    QWizardPage* userPage;
    QLabel*      u_lName;
    QLineEdit*   u_nameEdit;
    QLabel*      u_lLogin;
    QLineEdit*   u_loginEdit;
    QLabel*      u_lPass1;
    QLineEdit*   u_passEdit1;
    QLabel*      u_lPass2;
    QLineEdit*   u_passEdit2;
    QLabel*      u_lPassAccept;
    QPushButton* u_bGuestLogin;

    QWizardPage* databasePage;
    QLabel*      d_lHost;
    QLineEdit*   d_hostEdit;
    QLabel*      d_lPort;
    QLineEdit*   d_portEdit;
    QLabel*      d_lDbName;
    QLineEdit*   d_dbNameEdit;
    QPushButton* d_bCheckLogin;
    QLabel*      d_lCheckLogin;

    QWizardPage*  exitPage;
    QTextBrowser* e_sumBrowser;
    QLabel*       e_lHint;

    QLabel        conLabel;
    QString       conclusion;

    //! \brief Determines, if the closeEvent() was called by accept() or reject() finish funtion.
    bool saveClose;
    //! \brief Used for displaying text and using the right functions when clicking on button "Guest Login"
    bool guestLogin;
    //! \brief Result of the checkLoginData() function
    bool loginOk;
};
#endif // INTROWIZARD_H
