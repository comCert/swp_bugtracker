#include "topframe.h"

#include <QtGui>
#include "bug.h"
#include "bugtablemodel.h"
#include "user.h"
#include "session.h"
#include "database.h"
#include "dialog/optiondialog.h"
#include "widget/buglist.h"
#include "widget/buginfo.h"
#include "widget/fileviewer.h"
#include "dialog/userdialog.h"

Topframe::Topframe(QWidget *parent) :
    QMainWindow(parent)
{
    setWindowIcon(QIcon(":/png/bug"));
    hBrow = new HelpBrowser;
    readSettings();

    createMenus();
    createDock();
    createCentralWidget();
    createStatusBar();
}

Topframe::~Topframe()
{
    delete hBrow;
}

void Topframe::createMenus()
{
    QMenu   * menu;
    QAction * action;

    menu = menuBar()->addMenu(tr("Application")); // login of the menu to the top bar

    newBugAction = new QAction(tr("New Bug"), this);
    newBugAction->setIcon(QIcon(":/png/addDb"));
    newBugAction->setShortcuts(QKeySequence::New);
    connect(newBugAction, SIGNAL(triggered()), this, SLOT(createNewBug()));
    menu->addAction(newBugAction);

    newUserAction = new QAction(tr("New User"), this);
    newUserAction->setToolTip("Create a new user.");
    connect(newUserAction, SIGNAL(triggered()), this, SLOT(showUserDialog()));
    menu->addAction(newUserAction);

    action = new QAction(tr("Configure"), this);
    action->setIcon(QIcon(":/png/options"));
    connect(action, SIGNAL(triggered()), this, SLOT(showOptionDialog()));
    menu->addAction(action);

    action = new QAction(tr("Close"), this); // action definition
    action->setShortcuts(QKeySequence::Quit);
    connect(action, SIGNAL(triggered()), this, SLOT(close())); // look up signal-slot-concept
    menu->addSeparator();
    menu->addAction(action);

    menu = menuBar()->addMenu(tr("Help"));

    action = new QAction(tr("Show Documentation"), this);
    action->setShortcuts(QKeySequence::HelpContents);
    connect(action, SIGNAL(triggered()), this, SLOT(showDocumentation()));
    menu->addAction(action);

    action = new QAction(tr("About"), this);
    connect(action, SIGNAL(triggered()), this, SLOT(about()));
    menu->addAction(action);

    action = new QAction(tr("About Qt"), this);
    connect(action, SIGNAL(triggered()), qApp, SLOT(aboutQt()));
    menu->addAction(action);

    connect(&Session::instance, SIGNAL(loggedIn(User*)),
            this, SLOT(onLoggedIn(User*)));
}

int dockPosToInt(Qt::DockWidgetArea area)
{
    switch (area) {
    default : //falls through
    case Qt::LeftDockWidgetArea :
        return 0;
    case Qt::RightDockWidgetArea:
        return 1;
    case Qt::TopDockWidgetArea :
        return 2;
    case Qt::BottomDockWidgetArea :
        return 3;
    }
}

Qt::DockWidgetArea intToDockPos(int area)
{
    switch (area) {
    default : //falls through
    case 0 :
        return Qt::LeftDockWidgetArea;
    case 1:
        return Qt::RightDockWidgetArea;
    case 2 :
        return Qt::TopDockWidgetArea;
    case 3 :
        return Qt::BottomDockWidgetArea;
    }
}

void Topframe::createDock()
{
    QSettings conf("BugTracker", QSettings::NativeFormat);

    if (conf.value("general/dockShown", true).toBool() == false)
        return;

    dock = new Dock(this);
    connect(dock, SIGNAL(viewClicked()), this, SLOT(showBugOverview()));
    connect(dock, SIGNAL(listClicked()), this, SLOT(showBugList()));
    connect(dock, SIGNAL(nBugClicked()), this, SLOT(createNewBug()));
    addDockWidget(intToDockPos(conf.value("general/dockPosition").toInt()), dock);
}

void Topframe::createCentralWidget()
{
    tabWidget = new QTabWidget(this);
    tabWidget->setTabsClosable(true);
    tabWidget->setMovable(true);
    connect(tabWidget, SIGNAL(tabCloseRequested(int)),
            this     , SLOT(closeTab(int)));
    setCentralWidget(tabWidget); // set widget to center of application
}

void Topframe::createStatusBar()
{
    statusLabel = new QLabel;
    statusBar()->addPermanentWidget(statusLabel);
}

void Topframe::updateStatusBar()
{
    statusLabel->setText(tr("Logged in as: ")
        .append(Session::getUser().getUsername()));
}

void Topframe::showOptionDialog()
{
    OptionDialog dia(this);
    dia.exec();
}

void Topframe::createNewBug()
{
    BugDialog dia(this);
    dia.exec();

    // ~bugList->refresh();
}

void Topframe::onLoggedIn(User * u)
{
    newBugAction->setEnabled( u->hasPermission("bug.create") );
    newUserAction->setEnabled( u->hasPermission("user.create") );
    updateStatusBar();

    // close all tabs
    while(tabWidget->count())
        tabWidget->removeTab(0);

    showBugOverview();
    bugOverview->refresh();
    //showBugList();
}

void Topframe::showBugOverview()
{
    if(bugList == NULL)
    {
        bugOverview = new BugOverview;
        // ~connect(&Session::instance, SIGNAL(loggedIn(User*)),
                // ~bugOverview, SLOT(refresh(User*)));
        connect(bugOverview, SIGNAL(bugSelected(Bug*)),
                this, SLOT(showBugInfo(Bug*)));
    }

    // check if tab for buglist already exists
    BugOverview * view;
    int tc = tabWidget->count();
    for(int i = 0; i < tc; ++i)
    {
        view = qobject_cast<BugOverview *>(tabWidget->widget(i));
        if(view != NULL)
        {
            tabWidget->setCurrentIndex(i);
            return;
        }
    }

    // if not, create a new tab
    tabWidget->addTab(bugOverview, tr("&Bug Overview"));
    tabWidget->setTabToolTip(tc, tr("Your personal overview of current happenings"));
    tabWidget->setCurrentIndex(tc);
}

void Topframe::showBugList()
{
    if(bugList == NULL)
    {
        bugList = new BugList;
        connect(bugList, SIGNAL(bugSelected(Bug*)),
                this   , SLOT(showBugInfo(Bug*)));
    }

    // check if tab for buglist already exists
    BugList * list;
    int tc = tabWidget->count();
    for(int i = 0; i < tc; ++i)
    {
        list = qobject_cast<BugList *>(tabWidget->widget(i));
        if(list != NULL)
        {
            tabWidget->setCurrentIndex(i);
            return;
        }
    }

    // if not, create a new tab
    tabWidget->addTab(bugList, tr("&Bug List"));
    tabWidget->setTabToolTip(tc, tr("Your personal overview of current happenings"));
    tabWidget->setCurrentIndex(tc);
}

void Topframe::showBugInfo(Bug *b)
{
    BugInfo * info;

    // check if tab for bug already exists
    int tc = tabWidget->count();
    for(int i = 0; i < tc; ++i)
    {
        info = qobject_cast<BugInfo *>(tabWidget->widget(i));
        if(info != NULL && info->getBug()->getID() == b->getID())
        {
            tabWidget->setCurrentIndex(i);
            return;
        }
    }

    // if not, create a new tab
    info = new BugInfo(b, tabWidget);
    connect(info, SIGNAL(attachmentSelected(Attachment*)),
            this, SLOT(showAttachmentInfo(Attachment*)));
    tabWidget->addTab(info, tr("Bug #%1").arg(b->getID()));
    tabWidget->setTabToolTip(tc, b->getTitle());
    tabWidget->setCurrentIndex(tc);
}

void Topframe::showAttachmentInfo(Attachment *a)
{
    FileViewer * fv;

    // check if tab for bug already exists
    int tc = tabWidget->count();
    for(int i = 0; i < tc; ++i)
    {
        fv = qobject_cast<FileViewer *>(tabWidget->widget(i));
        if(fv != NULL && fv->getFile()->getID() == a->getID())
        {
            tabWidget->setCurrentIndex(i);
            return;
        }
    }

    // if not, create a new tab
    fv = new FileViewer(a, tabWidget);
    tabWidget->addTab(fv, tr("File #%1").arg(a->getID()));
    tabWidget->setTabToolTip(tc, a->getName());
    tabWidget->setCurrentIndex(tc);
}

void Topframe::about()
{
    QMessageBox::about(this, tr("BugTracker"),
                       tr("This is a project created by a team of students of the Technical"
                          "University of Chemnitz. The goal is a simple bug tracking software"
                          "which could be used in software development teams. It is written"
                          "in C++ by use of the Qt-framework.\n\r\n\r"
                          "Use it without any warrenty."));
}

void Topframe::closeTab(int index)
{
    tabWidget->removeTab(index);
}

void Topframe::showDocumentation()
{
    hBrow->showDocumentation("index.html");
}

void Topframe::showUserDialog()
{
    UserDialog dia;
    dia.exec();
}

void Topframe::closeEvent(QCloseEvent *e)
{
    QSettings conf("BugTracker", QSettings::NativeFormat);
    /*if (conf.value("general/askOnClose", true).toBool() == true) {
        QMessageBox::StandardButton ret;
        ret = QMessageBox::warning(this, tr("Warning: Close"),
                                   tr("Are you sure you want to quit the application?"),
                                   QMessageBox::No | QMessageBox::Yes); // QMessageBox::Cancel | QMessageBox::Close);
        // Kleinigkeit- davor waren beide Aktionen mit gleichem Shortcut
        // ich habe nicht gefunden wie man beim Standardbutton Close Label zu vielleicht "Quit" ändert
        // keine Ahnung ob No/Yes okay ist oder zu langweilig wirkt
        if (ret == QMessageBox::No) {
            e->ignore();
        } else if (ret == QMessageBox::Yes) {
            writeSettings();
            e->accept();
        }
    } else {
        writeSettings();
        e->accept();
    }*/
    /*
     * Ansatz a
     *
     * scheinbar gibt es aber einige Probleme in der Darstellung...
     */
    if (conf.value("general/askOnClose", true).toBool() == true) {
        QMessageBox ret;
        QPushButton *cancelButton = ret.addButton(QMessageBox::Cancel);
        QPushButton *quitButton   = ret.addButton(tr("&Quit"), QMessageBox::AcceptRole);
        ret.setWindowTitle(tr("Warning: Close"));
        ret.setText(tr("Are you sure you want to quit the application?"));
        // ~ret.setParent(this);

        ret.exec();

        if (ret.clickedButton() == cancelButton) {
            e->ignore();
        } else if (ret.clickedButton() == quitButton) {
            writeSettings();
            e->accept();
        }
    } else {
        writeSettings();
        e->accept();
    }
}

void Topframe::writeSettings()
{
    QSettings conf("BugTracker", QSettings::NativeFormat);
    conf.setValue("window/pos", pos());
    conf.setValue("window/size", size());
    conf.setValue("window/fullScreen", window()->isMaximized());
    conf.setValue("window/fullScreen", window()->isFullScreen());
    conf.setValue("general/dockPosition", dockPosToInt(dock->getDockPosition()));
    conf.setValue("general/dockShown", dock->isVisible());
}

void Topframe::readSettings()
{
    QSettings conf("BugTracker", QSettings::NativeFormat);
    if (conf.value("window/fullScreen", false).toBool() == true) {
        this->window()->setWindowState(Qt::WindowFullScreen);
    } else if (conf.value("window/maximized", false).toBool() == true) {
        this->window()->setWindowState(Qt::WindowMaximized);
    } else {
        this->window()->setWindowState(Qt::WindowNoState);
        QPoint pos = conf.value("window/pos", QPoint(200, 200)).toPoint();
        QSize size = conf.value("window/size", QSize(400, 400)).toSize();
        resize(size);
        move(pos);
    }
}
