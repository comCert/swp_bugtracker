#ifndef TOPFRAME_H
#define TOPFRAME_H

#include <QMainWindow>
#include <QGroupBox>
#include <QLabel>
#include <QPushButton>
#include <QAction>
#include <QWidget>
#include <QMenu>
#include <QTableView>
#include <QTabWidget>
#include <QTimer>
#include "widget/buglist.h"
#include "widget/bugoverview.h"
#include "widget/dock.h"
#include "util/helpbrowser.h"
#include "dialog/bugdialog.h"

class Bug;

class Topframe : public QMainWindow
{
    Q_OBJECT // makes the whole class(-object) effect aware
public:
    explicit Topframe(QWidget *parent = 0); /* Constructor */
    virtual ~Topframe();

private slots: /* read about the signal-slot-concept of Qt
                 - it's an expansion of c++ */
    void onLoggedIn(User *);
    void updateStatusBar();
    void showBugOverview();
    void showBugList();
    void showOptionDialog();
    void showUserDialog();
    void showBugInfo(Bug *);
    void showAttachmentInfo(Attachment *);
    void closeTab(int index);
    void createNewBug();
    void showDocumentation();
    void about();

protected:
    void closeEvent(QCloseEvent *e);

private:
    void createMenus();
    void createDock();
    void createCentralWidget();
    void createStatusBar();

    void handleLogin();
    void writeSettings();
    void readSettings();

    QAction * newBugAction;
    QAction * newUserAction;
    QLabel* statusLabel;
    QTabWidget* tabWidget;
    BugOverview * bugOverview;
    BugList* bugList;
    Dock* dock;
    HelpBrowser* hBrow;
};

#endif // TOPFRAME_H
