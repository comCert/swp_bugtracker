#include "introwizard.h"
#include "session.h"
#include <QPixmap>
#include <QStyle>

#include <QtGui>

IntroWizard::IntroWizard(QWidget* parent)
    : QWizard(parent)
{
    setWindowIcon(QIcon(":/png/bug"));
    setMinimumSize(400, 400);
    guestLogin = false;
    loginOk = false;
    setupPages();

    addPage(introPage);
    addPage(userPage);
    addPage(databasePage);
    addPage(exitPage);

    // could add some images to nice it up
    //QWizard::BackgroundPixmap
    //QWizard::BannerPixmap
    //QWizard::LogoPixmap
    //QWizard::WizardPixmap
    //QWizard::WatermarkPixmap
    QPixmap logo(":/img/noBug");
    setPixmap(QWizard::LogoPixmap, logo.scaled(50, 50));
    QPixmap mark(":/img/account");
    setPixmap(QWizard::WatermarkPixmap, mark.scaled(100, 100));

    connect(this, SIGNAL(currentIdChanged(int)), this, SLOT(generate(int)));
    connect(this, SIGNAL(helpRequested()), this, SLOT(help()));

    setWindowTitle(tr("Intro Wizard"));
    setOption(QWizard::HaveHelpButton, true);
    setOption(QWizard::NoBackButtonOnStartPage, true);


    button(QWizard::HelpButton)->setIcon(style()->standardIcon(QStyle::SP_DialogHelpButton));
    button(QWizard::CancelButton)->setIcon(style()->standardIcon(QStyle::SP_DialogCancelButton));
    button(QWizard::CancelButton)->setShortcut(QKeySequence(Qt::Key_C));
    button(QWizard::NextButton)->setIcon(style()->standardIcon(QStyle::SP_ArrowForward));
    button(QWizard::BackButton)->setIcon(style()->standardIcon(QStyle::SP_ArrowBack));
    button(QWizard::FinishButton)->setIcon(style()->standardIcon(QStyle::SP_DialogApplyButton));
}

void IntroWizard::accept()
{
    // must be emitted to ensuring correct finishing
    emit accepted();

    storeSettings();

    saveClose = true;
    close();
}

void IntroWizard::storeSettings()
{
    // store information into settings
    QSettings set("BugTracker", QSettings::NativeFormat);
    set.setValue("user/userName",   u_nameEdit->text());
    set.setValue("user/loginName",  u_loginEdit->text());
    set.setValue("user/loginPassword", u_passEdit1->text());
    set.setValue("connection/host", d_hostEdit->text());
    set.setValue("connection/port", d_portEdit->text().toInt());
    set.setValue("connection/db",   d_dbNameEdit->text());
    set.setValue("general/firstStart", false);
}

void IntroWizard::reject()
{
    // must be emitted to ensure correct finishing
    emit rejected();

    saveClose = false;
    close();
}

void IntroWizard::closeEvent(QCloseEvent *e)
{
    if (!saveClose) {
        QMessageBox ret;
        QPushButton *cancelButton = ret.addButton(QMessageBox::Cancel);
        QPushButton *quitButton   = ret.addButton(tr("&Quit"), QMessageBox::AcceptRole);
        ret.setWindowTitle(tr("Warning: Loss of Information"));
        ret.setText(tr("All given information will be lost if continuing!"));
        ret.exec();

        if (ret.clickedButton() == cancelButton) {
            e->ignore();
        } else if (ret.clickedButton() == quitButton) {
            e->accept();
        }
    } else {
        e->accept();
    }
}

void IntroWizard::help()
{
    switch(currentId()) {
    default: //fall through
    case 0 : // intro page
        QMessageBox::information(this, tr("Help: Intro"),
                                 tr("Here you will get help for your start."
                                    "Every page has it's own help popup, so request "
                                    "when there are questions."),
                                 QMessageBox::Ok);
        break;
    case 1 : // user page
        QMessageBox::information(this, tr("Help: User"),
                                 tr("Your name is your first and last name seperated with a space."
                                    "Your login name has no spaces or other values apart from characters."
                                    "The same with your password."),
                                 QMessageBox::Ok);
        break;
    case 2 : // connection page
        QMessageBox::information(this, tr("Help: Connection"),
                                 tr("The host is a number (ip address) or a name (url)."
                                    "The port is a number. It accepts only valid forms."
                                    "The database name should be a string like 'bugDB'."),
                                 QMessageBox::Ok);
        break;
    case 3 : // exit page
        QMessageBox::information(this, tr("Help: Conclusion"),
                                 tr("Here are your given informations. Please check them."),
                                 QMessageBox::Ok);
        break;
    }
}

void IntroWizard::fillGuestInfos()
{
    if (!guestLogin) {
        u_nameEdit->setText(tr(""));
        u_loginEdit->setText(tr("guest"));
        u_passEdit1->setText(tr("guest"));
        u_passEdit2->setText(tr("guest"));
        guestLogin = true;
        u_bGuestLogin->setText(tr("Deaktivate Guest Login"));
    } else {
        u_nameEdit->clear();
        u_loginEdit->clear();
        u_passEdit1->clear();
        u_passEdit2->clear();
        guestLogin = false;
        u_bGuestLogin->setText(tr("Guest Login"));
    }
}

void IntroWizard::checkLoginData()
{
    QPalette p;

    p.setColor(QPalette::WindowText, Qt::black);
    d_lCheckLogin->setPalette(p);
    d_lCheckLogin->setText(tr("checking login - please be patient ..."));

    // login
    storeSettings();
    loginOk = Session::login(u_loginEdit->text(), u_passEdit1->text());

    if (loginOk) {
        p.setColor(QPalette::WindowText, Qt::blue);
        d_lCheckLogin->setPalette(p);
        d_lCheckLogin->setText(tr("Success! Please confirm."));
    } else {
        p.setColor(QPalette::WindowText, Qt::red);
        d_lCheckLogin->setPalette(p);
        d_lCheckLogin->setText(tr("Error! Please check information."));
    }
}

void IntroWizard::generate(int id)
{
    if (id == 3)
        updateConclusions();
}

void IntroWizard::setupPages()
{
    // intro page
    introPage = new QWizardPage(this);
    introPage->setTitle(tr("Introduction assistant"));
    introPage->setSubTitle(tr("Your help to get it working"));
    introPage->setTitle(tr("Introduction"));
    i_l = new QLabel(tr("This wizard will guide you through the configuration "
                        "of the bug tracking software you use the first time. "
                        "During this initial process you require some login "
                        "information, which you should have get from your "
                        "administrator.\n\r\n\r"
                        "If you don't have any information described above, "
                        "cancel and ask for it in your company."));
    i_l->setWordWrap(true);
    QVBoxLayout* i_layout = new QVBoxLayout;
    i_layout->addWidget(i_l);
    introPage->setLayout(i_layout);

    // user page
    userPage = new QWizardPage(this);
    userPage->setTitle(tr("User information"));
    userPage->setSubTitle(tr("Login ingormation into the database"));
    QVBoxLayout* u_topLayout = new QVBoxLayout;
    QFormLayout* u_layout = new QFormLayout;
    QHBoxLayout* u_bLayout = new QHBoxLayout;
    u_lName = new QLabel(tr("Your name"), this);
    u_nameEdit = new QLineEdit(this);
    u_lLogin = new QLabel(tr("Login name"), this);
    u_loginEdit = new QLineEdit(this);
    u_lPass1 = new QLabel(tr("Password"), this);
    u_passEdit1 = new QLineEdit(this);
    u_passEdit1->setPlaceholderText(tr("hidden"));
    u_passEdit1->setEchoMode(QLineEdit::Password);
    connect(u_passEdit1, SIGNAL(textChanged(QString)), this, SLOT(updatePassLabel()));
    u_lPass2 = new QLabel(tr("Confirm password"), this);
    u_passEdit2 = new QLineEdit(this);
    u_passEdit2->setPlaceholderText(tr("hidden"));
    u_passEdit2->setEchoMode(QLineEdit::Password);
    connect(u_passEdit2, SIGNAL(textChanged(QString)), this, SLOT(updatePassLabel()));
    u_lPassAccept = new QLabel(this);
    u_bGuestLogin = new QPushButton(tr("Guest Login"), this);
    connect(u_bGuestLogin, SIGNAL(clicked()), this, SLOT(fillGuestInfos()));
    u_layout->addRow(u_lName, u_nameEdit);
    u_layout->addRow(u_lLogin, u_loginEdit);
    u_layout->addRow(u_lPass1, u_passEdit1);
    u_layout->addRow(u_lPass2, u_passEdit2);
    u_layout->addWidget(u_lPassAccept);
    u_bLayout->addStretch(1);
    u_bLayout->addWidget(u_bGuestLogin);
    u_topLayout->addLayout(u_layout);
    u_topLayout->addStretch(1);
    u_topLayout->addLayout(u_bLayout);
    userPage->setLayout(u_topLayout);

    // database page
    databasePage = new QWizardPage(this);
    databasePage->setTitle(tr("Connection information"));
    databasePage->setSubTitle(tr("Connection information into the database"));
    QVBoxLayout* d_topLayout = new QVBoxLayout;
    QFormLayout* d_layout = new QFormLayout;
    QHBoxLayout* d_b1Layout = new QHBoxLayout;
    QHBoxLayout* d_b2Layout = new QHBoxLayout;
    d_lHost = new QLabel(tr("Hostname or ip-address"), this);
    d_hostEdit = new QLineEdit(this);
    d_lPort = new QLabel(tr("Port"), this);
    d_portEdit = new QLineEdit(this);
    d_portEdit->setInputMask("900000"); // one number req., five more possible
    d_lDbName = new QLabel(tr("Database name"), this);
    d_dbNameEdit = new QLineEdit(this);
    d_bCheckLogin = new QPushButton(tr("Check Login"), this);
    d_bCheckLogin->setToolTip(tr("Check the given login information and try to connect to the database."));
    d_lCheckLogin = new QLabel(this);
    connect(d_bCheckLogin, SIGNAL(clicked()), this, SLOT(checkLoginData()));
    d_layout->addRow(d_lHost, d_hostEdit);
    d_layout->addRow(d_lPort, d_portEdit);
    d_layout->addRow(d_lDbName, d_dbNameEdit);
    d_b1Layout->addStretch(1);
    d_b1Layout->addWidget(d_bCheckLogin);
    d_b2Layout->addStretch(1);
    d_b2Layout->addWidget(d_lCheckLogin);
    d_topLayout->addLayout(d_layout);
    d_topLayout->addStretch(1);
    d_topLayout->addLayout(d_b1Layout);
    d_topLayout->addLayout(d_b2Layout);
    databasePage->setLayout(d_topLayout);

    // exit page
    exitPage = new QWizardPage(this);
    exitPage->setTitle(tr("Conclusion"));
    exitPage->setSubTitle(tr("Please check your submitted data before you finish"));
    QVBoxLayout* e_layout = new QVBoxLayout;
    e_sumBrowser = new QTextBrowser(this);
    e_lHint = new QLabel(this);
    e_layout->addWidget(e_sumBrowser);
    e_layout->addWidget(e_lHint);
    exitPage->setLayout(e_layout);
}

QString& IntroWizard::generateConclusion()
{
    int len = u_passEdit1->text().length();
    conclusion = "";

    conclusion += tr("Your name:\t\t");
    conclusion += u_nameEdit->text();
    conclusion += "\n";
    conclusion += tr("Login name:\t\t");
    conclusion += u_loginEdit->text();
    conclusion += "\n";
    conclusion += tr("password:\t\t");
    // hides the password, not plain text; correct length
    for (int i = 0; i < len; ++i)
        conclusion += "*";
    conclusion += "\n";
    conclusion += "length:\t\t";
    conclusion += len;
    conclusion += "\n";
    conclusion += "\n\n";
    conclusion += tr("hostname:\t\t");
    conclusion += d_hostEdit->text();
    conclusion += "\n";
    conclusion += tr("port:\t\t");
    conclusion += d_portEdit->text();
    conclusion += "\n";
    conclusion += tr("database name:\t");
    conclusion += d_dbNameEdit->text();
    conclusion += "\n";

    return conclusion;
}

QLabel& IntroWizard::getConclusionLabel()
{
    // very weak message
    conLabel.setParent(this);
    QPalette p;
    if (u_loginEdit->text().isEmpty() ||
            u_passEdit1->text().isEmpty() ||
            d_hostEdit->text().isEmpty() ||
            d_portEdit->text().isEmpty() ||
            d_dbNameEdit->text().isEmpty()) {
        p.setColor(QPalette::WindowText, Qt::red);
        conLabel.setText(tr("The given information might be not valid!"));
    } else {
        p.setColor(QPalette::WindowText, Qt::blue);
        conLabel.setText(tr("The given information might be valid!"));
    }
    conLabel.setPalette(p);

    return conLabel;
}

void IntroWizard::updatePassLabel()
{
    // displays, if the password: match, missmatch or empty
    QPalette p;

    if (u_passEdit1->text().isEmpty()) {
        p.setColor(QPalette::WindowText, Qt::red);
        u_lPassAccept->setText(tr("empty password not accepted"));
    } else if (u_passEdit1->text() == u_passEdit2->text()) {
        p.setColor(QPalette::WindowText, Qt::green);
        u_lPassAccept->setText(tr("passwords match"));
    } else {
        p.setColor(QPalette::WindowText, Qt::red);
        u_lPassAccept->setText(tr("passwords don't match"));
    }
    u_lPassAccept->setPalette(p);
}

void IntroWizard::updateConclusions()
{
    e_lHint->setText(getConclusionLabel().text());
    e_lHint->setPalette(getConclusionLabel().palette());
    e_sumBrowser->setText(generateConclusion());
}
