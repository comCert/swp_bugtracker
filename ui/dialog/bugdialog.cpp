#include "bugdialog.h"
#include "bug.h"
#include "session.h"

#include <QtGui>

BugDialog::BugDialog(QWidget *parent) :
    QDialog(parent)
{
    //int role = Session::getUser()->getType();

    lTitle = new QLabel(tr("Title"), this);
    titleEdit = new QLineEdit(this);
    lDescription = new QLabel(tr("Description"), this);
    descriptionEdit = new QTextEdit(this);
    connect(descriptionEdit, SIGNAL(textChanged()), this, SLOT(updateCounter()));
    lDescriptionCounter = new QLabel(tr("0/200"), this);
    QPalette p;
    p.setColor(QPalette::WindowText, Qt::gray);
    lDescriptionCounter->setPalette(p);
    lModule = new QLabel(tr("Module"), this);
    moduleEdit = new QLineEdit(this);
    lVersion = new QLabel(tr("Version"), this);
    versionEdit = new QLineEdit(this);
    lPriority = new QLabel(tr("Priority"), this);
    priorityBox = new QComboBox(this);
    priorityBox->addItem(toString(BP_LOW), BP_LOW);
    priorityBox->addItem(toString(BP_DEFAULT), BP_DEFAULT);
    priorityBox->addItem(toString(BP_HIGH), BP_HIGH);
    priorityBox->addItem(toString(BP_URGENT), BP_URGENT);
    priorityBox->setCurrentIndex(1);

    buttonBox = new QDialogButtonBox(QDialogButtonBox::Help |
                                     QDialogButtonBox::Cancel |
                                     QDialogButtonBox::Ok,
                                     Qt::Horizontal,
                                     this);
    connect(buttonBox, SIGNAL(accepted()), this, SLOT(submit()));
    connect(buttonBox, SIGNAL(rejected()), this, SLOT(close()));
    connect(buttonBox, SIGNAL(helpRequested()), this, SLOT(help()));

    QVBoxLayout* top = new QVBoxLayout;
    QFormLayout* layout = new QFormLayout;
    layout->addRow(lTitle, titleEdit);
    QVBoxLayout* l1 = new QVBoxLayout;
    l1->addWidget(descriptionEdit);
    QHBoxLayout* l2 = new QHBoxLayout;
    l2->addStretch(1);
    l2->addWidget(lDescriptionCounter);
    l1->addLayout(l2);
    layout->addRow(lDescription, l1);
    layout->addRow(lModule, moduleEdit);
    layout->addRow(lVersion, versionEdit);
    layout->addRow(lPriority, priorityBox);
    top->addLayout(layout);
    top->addWidget(buttonBox);
    setLayout(top);

    setWindowTitle(tr("Submit an untracked bug"));
    setWindowIcon(QIcon(":/png/addDb"));
}

void BugDialog::updateCounter()
{
    int count = descriptionEdit->toPlainText().length();
    QString str; str.setNum(count);

    QPalette p;
    if (count > 200) {
        p.setColor(QPalette::WindowText, Qt::red);
    } else {
        p.setColor(QPalette::WindowText, Qt::gray);
    }
    lDescriptionCounter->setPalette(p);
    lDescriptionCounter->setText(str + "/200");
}

void BugDialog::closeEvent(QCloseEvent *e)
{
    QMessageBox::StandardButton ret;
    if (!titleEdit->text().isEmpty() ||
            !descriptionEdit->toPlainText().isEmpty()) {
        ret = QMessageBox::warning(this, tr("Warning: Losing information"),
                              tr("If you continue, all your entered information will be lost!"),
                              QMessageBox::Cancel | QMessageBox::Close);
        if (ret == QMessageBox::Close) {
            e->accept();
        } else {
            e->ignore();
        }
    } else {
        e->accept();
    }
}

void BugDialog::help()
{
    QMessageBox::information(this, tr("Help: Bug-Form"),
                             tr("This is the form wherein you can offer information according to a new bug you spotted."
                                "Please keep your report short and informativ.\n\r\n\r"
                                "If there are unusable elements then you haven't the rights to fill this informations."
                                "\n\rPlease aquire the documentation for more help."),
                             QMessageBox::Ok);
}

void BugDialog::submit()
{
    QString title   = titleEdit->text();
    QString desc    = descriptionEdit->toPlainText();
    QString module  = moduleEdit->text();
    QString version = versionEdit->text();
    int bp = priorityBox->itemData( priorityBox->currentIndex() ).toInt();

    if(title.isEmpty() || desc.isEmpty() || module.isEmpty() || version.isEmpty())
        return;

    Bug * b = new Bug(title, desc, module, version, toBugPriority(bp));
    Session::getUser().reportBug(b);
    delete b;
    accept();
}
