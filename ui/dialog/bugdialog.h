#ifndef BUGDIALOG_H
#define BUGDIALOG_H

#include <QDialog>
#include <QDialogButtonBox>
#include <QCheckBox>
#include <QComboBox>
#include <QLineEdit>
#include <QTextEdit>
#include <QLabel>
#include <QDateTimeEdit>

#include <QSqlQueryModel>
#include <QSqlQuery>

#include <QDataWidgetMapper>
#include <QSqlRelationalTableModel>
#include <QSqlTableModel>
#include <QItemSelectionModel>

class BugDialog : public QDialog
{
    Q_OBJECT
public:
    explicit BugDialog(QWidget *parent = 0);

signals:

protected:
    void closeEvent(QCloseEvent *e);

private slots:
    void updateCounter();
    void help();
    void submit();

private:
    void fillRoleBoxes();
    QLabel*        lTitle;
    QLineEdit*     titleEdit;
    QLabel*        lDescription;
    QTextEdit*     descriptionEdit;
    QLabel*        lDescriptionCounter;
    QLabel*        lModule;
    QLineEdit*     moduleEdit;
    QLabel*        lVersion;
    QLineEdit*     versionEdit;
    QLabel*        lPriority;
    QComboBox*     priorityBox;

    QDialogButtonBox* buttonBox;

    QSqlQueryModel* devModel;
    QSqlQueryModel* tesModel;

    QSqlRelationalTableModel* model;
    QDataWidgetMapper*        mapper;
    QItemSelectionModel*      selectionModel;
};

#endif // BUGDIALOG_H
