#ifndef USERDIALOG_H
#define USERDIALOG_H

#include <QDialog>
#include <QLabel>
#include <QLineEdit>
#include <QDialogButtonBox>

#include "session.h"

class UserDialog : public QDialog
{
    Q_OBJECT
public:
    explicit UserDialog(QWidget *parent = 0);
    
signals:
    
public slots:
    void submit();
    void help();
    
private:
    QLineEdit* loginEdit;
    QLineEdit* passEdit;
    QComboBox* typeCombo;
    QDialogButtonBox* bBox;
};

#endif // USERDIALOG_H
