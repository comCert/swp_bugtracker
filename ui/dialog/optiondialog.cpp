#include "optiondialog.h"

#include <QtGui>

OptionDialog::OptionDialog(QWidget *parent) :
    QDialog(parent)
{
    setMinimumSize(400, 350);
    setWindowModality(Qt::WindowModal);
    setWindowIcon(QIcon(":/png/options"));

    setup();
    loadConfiguration();

    QVBoxLayout* layout = new QVBoxLayout;
    layout->addWidget(optionsBox);
    layout->addWidget(buttonBox);
    setLayout(layout);
}

void OptionDialog::buttonClicked(QAbstractButton *button)
{
    if (button == buttonBox->button(QDialogButtonBox::Ok)) {
        ok();
    } else if (button == buttonBox->button(QDialogButtonBox::Help)) {
        help();
    } else if (button == buttonBox->button(QDialogButtonBox::Apply)) {
        apply();
    } else {
        close();
    }
}

void OptionDialog::help()
{
    // make help active subslot aware - check if a good idea
    /*if (optionsBox->currentWidget() == generalWidget) {

    } else if (optionsBox->currentWidget() == userWidget) {

    } else if (optionsBox->currentWidget() == connectionWidget) {

    } else { // standart -- normally not used
        QMessageBox::information(this, tr("Help: Options Dialog"),
                                 tr("Within this form you can change some behaviour of the programm"),
                                 QMessageBox::Close);
    }*/

    // first draft
    QMessageBox::information(this, "Configuration Help",
                             "In this dialog you are able to change some settings of the application.\n\r\n\r"
                             "In the General tab you can change the behavoir of the application.\n\r"
                             "In the User tab you can check and change your login information or your name.\n\r"
                             "And in the Database tab you cann check your connection information to the database.\n\r",
                             QMessageBox::Close);
}

void OptionDialog::apply()
{
    storeConfiguration();
}

void OptionDialog::ok()
{
    storeConfiguration();
    close();
}

void OptionDialog::setup()
{
    optionsBox = new QToolBox(this);

    generalWidget = new QWidget(this);
    QVBoxLayout* gLayout = new QVBoxLayout;
    askOnCloseChecker = new QCheckBox(tr("Confirm closing the main window"), this);
    askOnCloseChecker->setToolTip(tr("If this option is checked, there will popup a small dialog, which"
                                     "asks you, if you are shure to close the application."));
    hasDockChecker = new QCheckBox(tr("Show dock in the main window"), this);
    hasDockChecker->setToolTip(tr("If this option is checked, the dock in the main window will be shown."
                                  "It offers you a quick access to some important functions to improve"
                                  "your workflow."));
    // ...
    gLayout->addWidget(askOnCloseChecker);
    gLayout->addWidget(hasDockChecker);
    gLayout->addStretch(1);
    generalWidget->setLayout(gLayout);

    userWidget = new QWidget(this);
    QFormLayout* uLayout = new QFormLayout;
    userNameEdit = new QLineEdit(this);
    userLoginEdit = new QLineEdit(this);
    userPassEdit = new QLineEdit(this);
    userPassEdit->setEchoMode(QLineEdit::PasswordEchoOnEdit);
    // ...
    uLayout->addRow(tr("Your name"), userNameEdit);
    uLayout->addRow(tr("Your login name"), userLoginEdit);
    uLayout->addRow(tr("Password"), userPassEdit);
    userWidget->setLayout(uLayout);

    connectionWidget = new QWidget(this);
    QFormLayout* cLayout = new QFormLayout;
    conHostEdit = new QLineEdit(this);
    conPortEdit = new QLineEdit(this);
    conPortEdit->setInputMask("900000");
    conNameEdit = new QLineEdit(this);
    // ...
    cLayout->addRow(tr("Hostname or IP-Address"), conHostEdit);
    cLayout->addRow(tr("Port"), conPortEdit);
    cLayout->addRow(tr("Database name"), conNameEdit);
    connectionWidget->setLayout(cLayout);

    optionsBox->addItem(generalWidget, tr("General"));
    optionsBox->setItemToolTip(0, tr("General settings of the application"));
    optionsBox->setItemIcon(0, QIcon(":/png/optGen"));
    optionsBox->addItem(userWidget, tr("User"));
    optionsBox->setItemToolTip(1, tr("User information"));
    optionsBox->setItemIcon(1, QIcon(":/png/optUsr"));
    optionsBox->addItem(connectionWidget, tr("Connection"));
    optionsBox->setItemToolTip(2, tr("Connection information"));
    optionsBox->setItemIcon(2, QIcon(":/png/optDb"));

    buttonBox = new QDialogButtonBox(QDialogButtonBox::Help
                                     | QDialogButtonBox::Ok
                                     | QDialogButtonBox::Apply
                                     | QDialogButtonBox::Cancel,
                                     Qt::Horizontal,
                                     this);
    connect(buttonBox, SIGNAL(clicked(QAbstractButton*)),
            this, SLOT(buttonClicked(QAbstractButton*)));
}

void OptionDialog::loadConfiguration()
{
    QSettings config("BugTracker", QSettings::NativeFormat);

    // general
    askOnCloseChecker->setChecked(config.value("general/askOnClose", true).toBool());
    hasDockChecker->setChecked(config.value("general/dockShown", true).toBool());

    // user
    userNameEdit->setText(config.value("user/userName").toString());
    userLoginEdit->setText(config.value("user/loginName").toString());
    userPassEdit->setText(config.value("user/loginPassword").toString());

    // connection
    conHostEdit->setText(config.value("connection/host").toString());
    conPortEdit->setText(config.value("connection/port").toString());
    conNameEdit->setText(config.value("connection/db").toString());
}

void OptionDialog::storeConfiguration()
{
    QSettings config("BugTracker", QSettings::NativeFormat);

    // general
    config.setValue("general/askOnClose", askOnCloseChecker->isChecked());
    config.setValue("general/dockShown", hasDockChecker->isChecked());

    // user
    config.setValue("user/userName", userNameEdit->text());
    config.setValue("user/loginName", userLoginEdit->text());
    //config.setValue("user/pass", userPassEdit->text());

    // connection
    config.setValue("connection/host", conHostEdit->text());
    config.setValue("connection/port", conPortEdit->text());
    config.setValue("connection/db", conNameEdit->text());
}
