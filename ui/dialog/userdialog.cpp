#include "userdialog.h"

#include <QtGui>
#include "user.h"

UserDialog::UserDialog(QWidget *parent) :
    QDialog(parent)
{
    loginEdit = new QLineEdit(this);
    passEdit = new QLineEdit(this);
    passEdit->setEchoMode(QLineEdit::PasswordEchoOnEdit);
    typeCombo = new QComboBox(this);
    typeCombo->addItem(tr("Guest"));
    typeCombo->addItem(tr("Administrator"));
    typeCombo->addItem(tr("Developer"));
    typeCombo->addItem(tr("Chief-Developer"));
    typeCombo->addItem(tr("Tester"));
    typeCombo->setCurrentIndex(0);
    bBox = new QDialogButtonBox(QDialogButtonBox::Help |
                                QDialogButtonBox::Ok |
                                QDialogButtonBox::Cancel,
                                Qt::Horizontal,
                                this);

    QFormLayout * layout = new QFormLayout;
    layout->addRow(tr("Login name"), loginEdit);
    layout->addRow(tr("Password"), passEdit);
    layout->addRow(tr("Type"), typeCombo);
    layout->addRow(bBox);

    connect(bBox, SIGNAL(accepted()), this, SLOT(submit()));
    connect(bBox, SIGNAL(rejected()), this, SLOT(close()));
    connect(bBox, SIGNAL(helpRequested()), this, SLOT(help()));

    setLayout(layout);
}

UserType typeMapping(int type)
{
    switch (type) {
    default: // fall through
    case UT_GUEST: return UT_GUEST;
    case UT_ADMIN: return UT_ADMIN;
    case UT_DEVELOPER: return UT_DEVELOPER;
    case UT_CHIEF_DEVELOPER: return UT_CHIEF_DEVELOPER;
    case UT_TESTER: return UT_TESTER;
    }
}

void UserDialog::submit()
{
    if (loginEdit->text().isEmpty() ||
            passEdit->text().isEmpty())
        QMessageBox::warning(this, tr("Missing Information"),
                             tr("Your information could not be submitted due to missing values!"),
                             QMessageBox::Ok | QMessageBox::NoButton);

    User* user;
    user = User::createUser(loginEdit->text(), passEdit->text(),
                            typeMapping(typeCombo->currentIndex()));

    if (user)
        close();
    else
        QMessageBox::critical(this, tr("Error on submitting!"),
                              tr("There was an error while submitting!"),
                              QMessageBox::Ok | QMessageBox::NoButton);
}

void UserDialog::help()
{
    QMessageBox::information(this, tr("Help: Add a new user"),
                             tr("Only administrators are allowed to do this.\n\r"
                                "The login name and password will be required to log into the database.\n\r"
                                "The user type specifies which rights the user gets."),
                             QMessageBox::Ok);
}
