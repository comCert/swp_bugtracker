#ifndef LOGINDIALOG_H
#define LOGINDIALOG_H

#include <QtGui>

class LoginDialog : public QDialog
{
    Q_OBJECT
private:
    QLineEdit * user, * pass;
private slots:
    void submit();
    void close();
public:
    LoginDialog();
    QString username() const { return user->text(); }
    QString password() const { return pass->text(); }
};

#endif /* LOGINDIALOG_H */
