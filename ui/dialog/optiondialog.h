#ifndef OPTIONDIALOG_H
#define OPTIONDIALOG_H

#include <QDialog>
#include <QWidget>
#include <QDialogButtonBox>
#include <QGroupBox>
#include <QCheckBox>
#include <QLineEdit>
#include <QLabel>
#include <QToolBox>

class OptionDialog : public QDialog
{
    Q_OBJECT
public:
    explicit OptionDialog(QWidget *parent = 0);

private slots:
    void buttonClicked(QAbstractButton* button);

protected:
    void help();
    void apply();
    void ok();

private:
    void setup();
    void loadConfiguration();
    void storeConfiguration();

    QToolBox*         optionsBox;
    QDialogButtonBox* buttonBox;
    QWidget*          generalWidget;
    QCheckBox*        askOnCloseChecker;
    QCheckBox*        hasDockChecker;

    QWidget*          userWidget;
    QLineEdit*        userNameEdit;
    QLineEdit*        userLoginEdit;
    QLineEdit*        userPassEdit;

    QWidget*          connectionWidget;
    QLineEdit*        conHostEdit;
    QLineEdit*        conPortEdit;
    QLineEdit*        conNameEdit;
};

#endif // OPTIONDIALOG_H
