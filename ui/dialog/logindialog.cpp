#include "logindialog.h"

LoginDialog::LoginDialog()
{
    QFormLayout * layout = new QFormLayout;
    QDialogButtonBox * bb = new QDialogButtonBox(QDialogButtonBox::Cancel |
                                     QDialogButtonBox::Ok,
                                     Qt::Horizontal,
                                     this);
    user = new QLineEdit(this);
    pass = new QLineEdit(this);
    pass->setEchoMode(QLineEdit::Password);
    connect(bb, SIGNAL(accepted()), this, SLOT(submit()));
    connect(bb, SIGNAL(rejected()), this, SLOT(close()));

    layout->addRow("Username", user);
    layout->addRow("Password", pass);
    layout->addRow(bb);
    this->setLayout(layout);
    this->setWindowTitle(tr("Login"));
}

void LoginDialog::submit()
{
    accept();
}

void LoginDialog::close()
{
    reject();
}

